<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns:v="urn:schemas-microsoft-com:vml">
   <head>
      <!-- Define Charset -->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <!-- Responsive Meta Tag -->
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
	  
      <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" />
      <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
   </head>
   <body yahoo="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#F7F8F9" style="font-family:sans-serif, Arial, Tahoma;">
      <!-- ======= main section ======= -->
      <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#F7F8F9" style=" background-size:100% 100%; background-position:center;" >
         <tr>
            <td height="50" style="font-size: 50px; line-height: 50px;"></td>
         </tr>
         <tr>
            <td>
               <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590 bodybg_color" style="border:1px solid #e8e9ea"> 
                  <tr>
                     <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <!-- ======= logo ======= -->
                     <td align="center">
                        <a href="#"><img src="" style="padding-left:5%;height: 50px;"></a>
                     </td>
                  </tr>
                   <tr>
                     <td height="20" style="font-size: 20px; line-height:20px;">&nbsp;</td>
                  </tr>
				   </table>
            </td>
         </tr>
        
         <tr>
            <td>
               <table border="0" width="600" align="center" cellpadding="0" cellspacing="0" class="container590" bgcolor="ffffff" style="border:1px solid #e8e9ea;border-top:0px!important;padding:40px">
                   <tr>
                     <td align="left" style="font-family: sans-serif, Arial, Tahoma;color:#000;font-size: 23px; line-height: 32px; "> 
                      Hi Admin,
                     </td>
                  </tr>
                 <tr>
                     <td height="25" style="font-size: 25px; line-height: 25px;"> </td>
                  </tr>
                       <tr>
                     <td align="left" style="font-family: sans-serif, Arial, Tahoma;color:#000;font-size: 18px; line-height: 26px; "> 
                       <span style="color:grey">{{ $user_data['first_name'] }}</span>, Send a Message to You.
                     </td>
                  </tr>
                  
                  <tr>
                     <td height="10" style="font-size: 10px; line-height:10px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td height="17" style="font-size: 13px; line-height: 18px;font-family: sans-serif, Arial, Tahoma;color:#525252">
                        <span style="font-size: 16px;" >{{user_data['$messages']}}</span>
					 </td>
                  </tr>
				   
				   
               </table>
            </td>
         </tr>
         <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
         </tr>
         <tr>
            <td>
               <table border="0" width="600" align="center" cellpadding="0" cellspacing="0" class="container590">
			     <tr>
                     
                  </tr>
				   <tr>
                       <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                   </tr>
                  <tr>
                     <td align="center" style="color:#BBB; font-size: 12px; font-family:sans-serif, Arial, Tahoma; mso-line-height-rule: exactly; line-height: 18px;" >
                         ClinySoft. All Rights Reserved.
						 
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
		  <tr>
            <td height="50" style="font-size: 50px; line-height: 50px;"></td>
         </tr>
         
      </table>
      <!-- ======= end header ======= -->
   </body>
</html>