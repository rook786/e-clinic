<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**************************************  Routes for UserTypeController 1.0 Starts   ********************************** */

Route::post('v1/user/type',                             'api\UserTypeController@store');                    //Route 1.1
Route::get('v1/user/types',                             'api\UserTypeController@get_list');                 //Route 1.2
Route::put('v1/user/type/{id}',                         'api\UserTypeController@update');                   //Route 1.3
Route::delete('v1/user/type/{id}',                      'api\UserTypeController@destroy');                  //Route 1.4
Route::put('v1/user/type/status/{id}',                  'api\UserTypeController@update_status');            //Route 1.5

/**************************************  Routes for UserDocumentTypeController 1.0 Ends   ************************************ */




/**************************************  Routes for DepartmentController 2.0 Starts   ********************************** */

Route::post('v1/department',                            'api\DepartmentController@store');                  //Route 2.1
Route::get('v1/departments',                            'api\DepartmentController@get_list');               //Route 2.2
Route::put('v1/department/{id}',                        'api\DepartmentController@update');                 //Route 2.3
Route::delete('v1/department/{id}',                     'api\DepartmentController@destroy');                //Route 2.4
Route::put('v1/department/status/{id}',                 'api\DepartmentController@update_status');          //Route 2.5

/**************************************  Routes for DepartmentController 2.0 Ends   ************************************ */




/**************************************  Routes for UserDocumentTypeController 3.0 Starts   ********************************** */

Route::post('v1/user/document/type',                    'api\UserDocumentTypeController@store');            //Route 3.1
Route::get('v1/user/document/types',                    'api\UserDocumentTypeController@get_list');         //Route 3.2
Route::put('v1/user/document/type/{id}',                'api\UserDocumentTypeController@update');           //Route 3.3
Route::delete('v1/user/document/type/{id}',             'api\UserDocumentTypeController@destroy');          //Route 3.4
Route::put('v1/user/document/type/status/{id}',         'api\UserDocumentTypeController@update_status');    //Route 3.5

/**************************************  Routes for UserDocumentTypeController 3.0 Ends   ************************************ */





/**************************************  Routes for SocialMediaTypeController 4.0 Starts   ********************************** */

Route::post('v1/social/media/type',                    'api\SocialMediaTypeController@store');              //Route 4.1
Route::get('v1/social/media/types',                    'api\SocialMediaTypeController@get_list');           //Route 4.2
Route::put('v1/social/media/type/{id}',                'api\SocialMediaTypeController@update');             //Route 4.3
Route::delete('v1/social/media/type/{id}',             'api\SocialMediaTypeController@destroy');            //Route 4.4
Route::put('v1/social/media/type/status/{id}',         'api\SocialMediaTypeController@update_status');      //Route 4.5

/**************************************  Routes for SocialMediaTypeController 4.0 Ends   ************************************ */




/**************************************  Routes for OpdTypeController 5.0 Starts   ********************************** */

Route::post('v1/opd/type',                              'api\OpdTypeController@store');                      //Route 5.1
Route::get('v1/opd/types',                              'api\OpdTypeController@get_list');                   //Route 5.2
Route::put('v1/opd/type/{id}',                          'api\OpdTypeController@update');                     //Route 5.3
Route::delete('v1/opd/type/{id}',                       'api\OpdTypeController@destroy');                    //Route 5.4
Route::put('v1/opd/type/status/{id}',                   'api\OpdTypeController@update_status');              //Route 5.5

/**************************************  Routes for OpdTypeController 5.0 Ends   ************************************ */




/**************************************  Routes for WeekDayController 6.0 Starts   ********************************** */

Route::post('v1/week/day',                              'api\WeekDayController@store');                      //Route 6.1
Route::get('v1/week/days',                              'api\WeekDayController@get_list');                   //Route 6.2
Route::put('v1/week/day/{id}',                          'api\WeekDayController@update');                     //Route 6.3
Route::delete('v1/week/day/{id}',                       'api\WeekDayController@destroy');                    //Route 6.4
Route::put('v1/week/day/status/{id}',                   'api\WeekDayController@update_status');              //Route 6.5

/**************************************  Routes for WeekDayController 6.0 Ends   ************************************ */




/**************************************  Routes for LeaveTypeController 7.0 Starts   ********************************** */

Route::post('v1/leave/type',                             'api\LeaveTypeController@store');                    //Route 7.1
Route::get('v1/leave/types',                             'api\LeaveTypeController@get_list');                 //Route 7.2
Route::put('v1/leave/type/{id}',                         'api\LeaveTypeController@update');                   //Route 7.3
Route::delete('v1/leave/type/{id}',                      'api\LeaveTypeController@destroy');                  //Route 7.4
Route::put('v1/leave/type/status/{id}',                  'api\LeaveTypeController@update_status');            //Route 7.5

/**************************************  Routes for LeaveTypeController 7.0 Ends   ************************************ */




/**************************************  Routes for VehicleTypeController 8.0 Starts   ********************************** */

Route::post('v1/vehicle/type',                             'api\VehicleTypeController@store');                 //Route 8.1
Route::get('v1/vehicle/types',                             'api\VehicleTypeController@get_list');              //Route 8.2
Route::put('v1/vehicle/type/{id}',                         'api\VehicleTypeController@update');                //Route 8.3
Route::delete('v1/vehicle/type/{id}',                      'api\VehicleTypeController@destroy');               //Route 8.4
Route::put('v1/vehicle/type/status/{id}',                  'api\VehicleTypeController@update_status');         //Route 8.5

/**************************************  Routes for VehicleTypeController 8.0 Ends   ************************************ */




/**************************************  Routes for VisitorTypeController 9.0 Starts   ********************************** */

Route::post('v1/visitor/type',                             'api\VisitorTypeController@store');                 //Route 9.1
Route::get('v1/visitor/types',                             'api\VisitorTypeController@get_list');              //Route 9.2
Route::put('v1/visitor/type/{id}',                         'api\VisitorTypeController@update');                //Route 9.3
Route::delete('v1/visitor/type/{id}',                      'api\VisitorTypeController@destroy');               //Route 9.4
Route::put('v1/visitor/type/status/{id}',                  'api\VisitorTypeController@update_status');         //Route 9.5

/**************************************  Routes for VisitorTypeController 9.0 Ends   ************************************ */




/**************************************  Routes for AchievementCategoryController 10.0 Starts   ********************************** */

Route::post('v1/achievement/category',                     'api\AchievementCategoryController@store');              //Route 10.1
Route::get('v1/achievement/categories',                    'api\AchievementCategoryController@get_list');           //Route 10.2
Route::put('v1/achievement/category/{id}',                 'api\AchievementCategoryController@update');             //Route 10.3
Route::delete('v1/achievement/category/{id}',              'api\AchievementCategoryController@destroy');            //Route 10.4
Route::put('v1/achievement/category/status/{id}',          'api\AchievementCategoryController@update_status');      //Route 10.5

/**************************************  Routes for AchievementCategoryController 10.0 Ends   ************************************ */




/**************************************  Routes for AchievementTypeController 11.0 Starts   ********************************** */

Route::post('v1/achievement/type',                          'api\AchievementTypeController@store');              //Route 11.1
Route::get('v1/achievement/types',                          'api\AchievementTypeController@get_list');           //Route 11.2
Route::put('v1/achievement/type/{id}',                      'api\AchievementTypeController@update');             //Route 11.3
Route::delete('v1/achievement/type/{id}',                   'api\AchievementTypeController@destroy');            //Route 11.4
Route::put('v1/achievement/type/status/{id}',               'api\AchievementTypeController@update_status');      //Route 11.5

/**************************************  Routes for AchievementCategoryController 11.0 Ends   ************************************ */




/**************************************  Routes for ShiftTypeController 12.0 Starts   ********************************** */

Route::post('v1/shift/type',                                'api\ShiftTypeController@store');              //Route 12.1
Route::get('v1/shift/types',                                'api\ShiftTypeController@get_list');           //Route 12.2
Route::put('v1/shift/type/{id}',                            'api\ShiftTypeController@update');             //Route 12.3
Route::delete('v1/shift/type/{id}',                         'api\ShiftTypeController@destroy');            //Route 12.4
Route::put('v1/shift/type/status/{id}',                     'api\ShiftTypeController@update_status');      //Route 12.5

/**************************************  Routes for ShiftTypeController 12.0 Ends   ************************************ */




/**************************************  Routes for VisitorDetailController 13.0 Starts   ********************************** */

Route::post('v1/visitor',                                   'api\VisitorDetailController@store');              //Route 13.1
Route::get('v1/visitors',                                   'api\VisitorDetailController@get_list');           //Route 13.2
Route::put('v1/visitor/{id}',                               'api\VisitorDetailController@update');             //Route 13.3
Route::delete('v1/visitor/{id}',                            'api\VisitorDetailController@destroy');            //Route 13.4

/**************************************  Routes for VisitorDetailController 13.0 Ends   ************************************ */




/**************************************  Routes for StaffVehicleDetailController 14.0 Starts   ********************************** */

Route::post('v1/staff/vehicle',                              'api\StaffVehicleDetailController@store');          //Route 14.1
Route::get('v1/staff/vehicles',                              'api\StaffVehicleDetailController@get_list');       //Route 14.2
Route::put('v1/staff/vehicle/{id}',                          'api\StaffVehicleDetailController@update');         //Route 14.3
Route::delete('v1/staff/vehicle/{id}',                       'api\StaffVehicleDetailController@destroy');        //Route 14.4

/**************************************  Routes for StaffVehicleDetailController 13.0 Ends   ************************************ */





/**************************************  Routes for StaffLeaveController 15.0 Starts   ********************************** */

Route::post('v1/staff/leave',                               'api\StaffLeaveController@store');          //Route 15.1
Route::get('v1/staff/leaves',                               'api\StaffLeaveController@get_list');       //Route 15.2
Route::put('v1/staff/leave/{id}',                           'api\StaffLeaveController@update');         //Route 15.3
Route::delete('v1/staff/leave/{id}',                        'api\StaffLeaveController@destroy');        //Route 15.4

/**************************************  Routes for StaffLeaveController 15.0 Ends   ************************************ */





/**************************************  Routes for UserAchievementController 16.0 Starts   ********************************** */

Route::post('v1/user/achievement',                          'api\UserAchievementController@store');                     //Route 16.1
Route::get('v1/user/achievements',                          'api\UserAchievementController@get_list');                  //Route 16.2
Route::put('v1/user/achievement/{id}',                      'api\UserAchievementController@update');                    //Route 16.3
Route::delete('v1/user/achievement/{id}',                   'api\UserAchievementController@destroy');                   //Route 16.4
Route::post('v1/user/achievement/upload',                   'api\UserAchievementController@upload_achievement_image');  //Route 16.5

/**************************************  Routes for UserAchievementController 16.0 Ends   ************************************ */




/**************************************  Routes for ShiftDetailController 17.0 Starts   ********************************** */

Route::post('v1/user/shift',                                'api\ShiftDetailController@store');                     //Route 17.1
Route::get('v1/user/shifts',                                'api\ShiftDetailController@get_list');                  //Route 17.2
Route::put('v1/user/shift/{id}',                            'api\ShiftDetailController@update');                    //Route 17.3
Route::delete('v1/user/shift/{id}',                         'api\ShiftDetailController@destroy');                   //Route 17.4

/**************************************  Routes for ShiftDetailController 17.0 Ends   ************************************ */




/**************************************  Routes for OpdDetailController 18.0 Starts   ********************************** */

Route::post('v1/opd',                                       'api\OpdDetailController@store');                     //Route 18.1
Route::get('v1/opds',                                       'api\OpdDetailController@get_list');                  //Route 18.2
Route::put('v1/opd/{id}',                                   'api\OpdDetailController@update');                    //Route 18.3
Route::delete('v1/opd/{id}',                                'api\OpdDetailController@destroy');                   //Route 18.4
Route::put('v1/opd/status/{id}',                            'api\OpdDetailController@update_status');             //Route 18.5

/**************************************  Routes for OpdDetailController 18.0 Ends   ************************************ */




/**************************************  Routes for VisitingOpdDetailController 19.0 Starts   ********************************** */

Route::post('v1/visiting/opd',                              'api\VisitingOpdDetailController@store');                     //Route 19.1
Route::get('v1/visiting/opds',                              'api\VisitingOpdDetailController@get_list');                  //Route 19.2
Route::put('v1/visiting/opd/{id}',                          'api\VisitingOpdDetailController@update');                    //Route 19.3
Route::delete('v1/visiting/opd/{id}',                       'api\VisitingOpdDetailController@destroy');                   //Route 19.4
Route::put('v1/visiting/opd/status/{id}',                   'api\VisitingOpdDetailController@update_status');             //Route 19.5

/**************************************  Routes for VisitingOpdDetailController 19.0 Ends   ************************************ */




/**************************************  Routes for VisitingStaffController 20.0 Starts   ********************************** */

Route::post('v1/visiting/staff',                              'api\VisitingStaffController@store');                     //Route 20.1
Route::get('v1/visiting/staffs',                              'api\VisitingStaffController@get_list');                  //Route 20.2
Route::put('v1/visiting/staff/{id}',                          'api\VisitingStaffController@update');                    //Route 20.3
Route::delete('v1/visiting/staff/{id}',                       'api\VisitingStaffController@destroy');                   //Route 20.4
Route::put('v1/visiting/staff/status/{id}',                   'api\VisitingStaffController@update_status');             //Route 20.5

/**************************************  Routes for VisitingStaffController 20.0 Ends   ************************************ */





/**************************************  Routes for UserDocumentController 21.0 Starts   ********************************** */

Route::post('v1/user/document',                                'api\UserDocumentController@store');                     //Route 21.1
Route::get('v1/user/documents',                                'api\UserDocumentController@get_list');                  //Route 21.2
Route::put('v1/user/document/{id}',                            'api\UserDocumentController@update');                    //Route 21.3
Route::delete('v1/user/document/{id}',                         'api\UserDocumentController@destroy');                   //Route 21.4
Route::post('v1/user/document/upload',                         'api\UserDocumentController@upload_document_image');     //Route 21.5

/**************************************  Routes for UserDocumentController 21.0 Ends   ************************************ */




/**************************************  Routes for DesignationController 22.0 Starts   ********************************** */

Route::post('v1/designation',                                   'api\DesignationController@store');                    //Route 22.1
Route::get('v1/designations',                                   'api\DesignationController@get_list');                 //Route 22.2
Route::put('v1/designation/{id}',                               'api\DesignationController@update');                   //Route 22.3
Route::delete('v1/designation/{id}',                            'api\DesignationController@destroy');                  //Route 22.4
Route::put('v1/designation/status/{id}',                        'api\DesignationController@update_status');            //Route 22.5

/**************************************  Routes for DesignationController 22.0 Ends   ************************************ */





/**************************************  Routes for UserController 23.0 Starts   ********************************** */

Route::post('v1/user',                                          'api\UserController@store');                     //Route 23.1
Route::get('v1/users',                                          'api\UserController@get_list');                  //Route 23.2
Route::put('v1/user/{id}',                                      'api\UserController@update');                    //Route 23.3
//Route::delete('v1/user/{id}',                                 'api\UserController@destroy');                   //Route 23.4
Route::put('v1/user/status/{id}',                               'api\UserController@update_status');             //Route 23.5
Route::get('v1/user/profile/{id}',                              'api\UserController@get_user_detail');           //Route 23.6
Route::put('v1/user/detail/{id}',                               'api\UserController@update_user_profile');       //Route 23.7
Route::post('v1/user/profile_image/upload',                     'api\UserController@upload_profile_image');      //Route 23.8

/**************************************  Routes for UserController 23.0 Ends   ************************************ */





/**************************************  Routes for LoginController 24.0 Starts   ********************************** */

Route::post('v1/user/login',                                    'api\LoginController@login');                     //Route 24.1
Route::put('v1/user/change/password/{id}',                      'api\LoginController@changePassword');            //Route 24.2
Route::post('v1/user/reset/password/email',                     'api\LoginController@forgotPasswordEmail');       //Route 24.3
Route::post('v1/user/verify/otp',                               'api\LoginController@verify_otp');                //Route 24.3
Route::put('v1/user/reset/password',                            'api\LoginController@forgotPasswordChange');      //Route 24.5

/**************************************  Routes for LoginController 24.0 Ends   ************************************ */






/**************************************  Routes for UserEducationDetailController 25.0 Starts   ********************************** */

Route::post('v1/user/education/detail',                         'api\UserEducationDetailController@store');              //Route 25.1
Route::get('v1/user/education/details',                         'api\UserEducationDetailController@get_list');           //Route 25.2
Route::put('v1/user/education/detail/{id}',                     'api\UserEducationDetailController@update');             //Route 25.3
Route::delete('v1/user/education/detail/{id}',                  'api\UserEducationDetailController@destroy');            //Route 25.4

/**************************************  Routes for UserEducationDetailController 25.0 Ends   ************************************ */




/**************************************  Routes for UserProfessionalDetailController 26.0 Starts   ********************************** */

Route::post('v1/user/professional/detail',                       'api\UserProfessionalDetailController@store');             //Route 26.1
Route::get('v1/user/professional/details',                       'api\UserProfessionalDetailController@get_list');          //Route 26.2
Route::put('v1/user/professional/detail/{id}',                   'api\UserProfessionalDetailController@update');            //Route 26.3
Route::delete('v1/user/professional/detail/{id}',                'api\UserProfessionalDetailController@destroy');           //Route 26.4

/**************************************  Routes for UserProfessionalDetailController 26.0 Ends   ************************************ */




/**************************************  Routes for StaffCategoryController 27.0 Starts   ********************************** */

Route::post('v1/staff/category',                             'api\StaffCategoryController@store');                    //Route 27.1
Route::get('v1/staff/categories',                            'api\StaffCategoryController@get_list');                 //Route 27.2
Route::put('v1/staff/category/{id}',                         'api\StaffCategoryController@update');                   //Route 27.3
Route::delete('v1/staff/category/{id}',                      'api\StaffCategoryController@destroy');                  //Route 27.4
Route::put('v1/staff/category/status/{id}',                  'api\StaffCategoryController@update_status');            //Route 27.5

/**************************************  Routes for StaffCategoryController 27.0 Ends   ************************************ */




/**************************************  Routes for WardDetailController 28.0 Starts   ********************************** */

Route::post('v1/ward/detail',                               'api\WardDetailController@store');                    //Route 28.1
Route::get('v1/ward/details',                               'api\WardDetailController@get_list');                 //Route 28.2
Route::put('v1/ward/detail/{id}',                           'api\WardDetailController@update');                   //Route 28.3
Route::delete('v1/ward/detail/{id}',                        'api\WardDetailController@destroy');                  //Route 28.4
Route::put('v1/ward/detail/status/{id}',                    'api\WardDetailController@update_status');            //Route 28.5

/**************************************  Routes for WardDetailController 28.0 Ends   ************************************ */





/**************************************  Routes for WardAssignmentController 29.0 Starts   ********************************** */

Route::post('v1/ward/assignment',                              'api\WardAssignmentController@store');                    //Route 28.1
Route::get('v1/ward/assignments',                              'api\WardAssignmentController@get_list');                 //Route 28.2
Route::put('v1/ward/assignment/{id}',                          'api\WardAssignmentController@update');                   //Route 28.3
Route::delete('v1/ward/assignment/{id}',                       'api\WardAssignmentController@destroy');                  //Route 28.4

/**************************************  Routes for WardDetailController 28.0 Ends   ************************************ */