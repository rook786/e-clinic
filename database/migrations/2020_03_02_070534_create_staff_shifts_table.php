<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');
            $table->string('department');
            $table->string('description');
            $table->string('shift_start_time');
            $table->string('shift_end_time');
            $table->string('break_start_time');
            $table->string('break_end_time');
            $table->string('venue');
            $table->string('staff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_shifts');
    }
}
