<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpdDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opd_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('opd_type');
            $table->string('week_day');
            $table->string('user_id');
            $table->string('open_time');
            $table->string('close_time');
            $table->string('break_start_time');
            $table->string('break_end_time');
            $table->string('venue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opd_details');
    }
}
