<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSalaryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_salary_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('basic_salary');
            $table->string('dearness_allowance');
            $table->string('house_rent_allowance');
            $table->string('leave_travel_allowance');
            $table->string('conveyance_allowance');
            $table->string('medical_allowance');
            $table->string('education_allowance');
            $table->string('hostel_allowance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_salary_details');
    }
}
