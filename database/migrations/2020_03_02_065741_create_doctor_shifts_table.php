<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');
            $table->string('department');
            $table->string('description');
            $table->string('shift_start_time');
            $table->string('shift_end_time');
            $table->string('break_start_time');
            $table->string('break_end_time');
            $table->string('venue');
            $table->string('doctors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_shifts');
    }
}
