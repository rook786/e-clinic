<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffVehicleDetail extends Model
{
    protected $fillable = ['user_id','vehicle_type','company_name','model','color','registration_no'];
}
