<?php
  namespace App\Traits;

  trait notification
  {
      protected function notification($name)
      {
          return $name;
      }
      protected function send_push_notification( $notification_type, $id, $title , $sub_title , $notification_token , $details = [] )
      {
          
         $data = array( 'title' => $title , 'subtitle' => $sub_title , 'ios_sound'=>'','badge'=>0 , 'details'=>$details, 'id' => $id, 'notification_type' => 
          $notification_type );
          $registration_ids = $notification_token;

//commented By R

          $new_notification_tokens = array();

          for ($i=0; $i < count($notification_token) ; $i++) 
          { 
            if($notification_token[$i] != "" || $notification_token[$i] != null)  {   
               
              $new_notification_tokens[] =   $notification_token[$i];   
            }
          }

          $content = array( "en" => $sub_title);
         $title = array( "en" => $title );
         $sub_title = array( "en" => '' );
         $fields = array(
                         'app_id' =>env("ONE_SIGNAL_APP_ID", ""),
                          'include_player_ids' => $new_notification_tokens,
                          'data' => $data,
                          'contents' => $content,
                          'headings' => $title,
                          'subtitle' => $sub_title,
                           'ios_sound'=>'incoming.caf',
                          'android_sound'=>'alert.mp3',
                          'ios_badgeCount'=>1,
                          'ios_badgeType'=>'Increase'

                         );
         $fields = json_encode($fields);
        //print("\nJSON sent:\n");
        //print($fields);
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic '.env("ONE_SIGNAL_REST_KEY", "")));
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
         curl_setopt($ch, CURLOPT_HEADER, FALSE);
         curl_setopt($ch, CURLOPT_POST, TRUE);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         $response = curl_exec($ch);
         curl_close($ch);
         $return["allresponses"] = $response;
         $return = json_encode( $return);
        //echo $return;
         
      }





      protected function notification_to_single_identifier($authToken , $title , $text , $details , $type, $id){
        $data = array( 'title' => $title, 'subtitle' => $text , 'ios_sound'=>'','badge'=>0,'details'=>$details , 'notification_type'=>$type,'id' => $id);
        $registration_ids = $authToken;

        $new_notification_tokens = array();

        for ($i=0; $i < count($authToken) ; $i++) 
        { 
          if($authToken[$i] != "" || $authToken[$i] != null)  {   
             
            $new_notification_tokens[] =   $authToken[$i];   
          }
        }

        $content = array( "en" => $text);
        $title = array( "en" => $title );
        $subTitle = array( "en" => '' );
        $fields = array(
                        'app_id' =>env("ONE_SIGNAL_APP_ID", ""),
                         'include_player_ids' => $new_notification_tokens,
                         'data' => $data,
                         'contents' => $content,
                         'headings' => $title,
                         'subtitle' => $subTitle,
               'notification_type' => $type,
                'ios_sound'=>'incoming.caf',
               'android_sound'=>'alert.mp3',
               'ios_badgeCount'=>1,
                   'ios_badgeType'=>'Increase'
                       
                        );
        $fields = json_encode($fields);
        //print("\nJSON sent:\n");
        //print($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic '.env("ONE_SIGNAL_REST_KEY", "")));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $return["allresponses"] = $response;
        $return = json_encode( $return);
        //echo $return;
      }





      protected function notification_to_single_identifier_silent($authToken , $title , $text , $details , $identifier)
      {
           
        $details['notification_type'] = $identifier;
        $data = array( 'title' => $title, 'subtitle' => $text , 'ios_sound'=>'','badge'=>0,'details'=>$details , 'notification_type'=>$identifier , 'content_available'=>true  );
        $registration_ids = $authToken;

        $new_notification_tokens = array();

          for ($i=0; $i < count($authToken) ; $i++) 
          { 
            if($authToken[$i] != "" || $authToken[$i] != null)  {   
               
              $new_notification_tokens[] =   $authToken[$i];   
            }
          }

        $content = array( "en" => $text);
        $title = array( "en" => $title );
        $subTitle = array( "en" => '' );
        $fields = array(
                          'app_id' =>env("ONE_SIGNAL_APP_ID", ""),
                          'include_player_ids' => $new_notification_tokens, // for single array($authToken)
                          'data' => $data,
                          //'contents' => $content,
                          //'headings' => $title,
                          'content_available' => true,
                          'content-available' => true,
                        
                          'notification_identifier' => $identifier,
                          'ios_sound'=>'incoming.caf',
                          'android_sound'=>'alert.mp3',
                     
                         
                    
                          '_background_data'=>true
                       
                        );
        $fields = json_encode($fields);
       //print("\nJSON sent:\n");
        //print($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic '.env("ONE_SIGNAL_REST_KEY", "")));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        //$return["allresponses"] = $response;
       // $return = json_encode( $return);
      //  echo $return;
     }
  }    