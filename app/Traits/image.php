<?php
namespace App\Traits;
use App;
use File;
use App\Traits\response;
// use Storage;
use Illuminate\Support\Facades\Storage;

trait image
{
    
    use response;
    
    
    protected function image($name)
    {
        return $name;
    }


    public function make_thumb($src, $dest, $desired_width)
    {

        /* read the source image */

        $width                                               =  @imagesx($source_image);
        $height                                              =  @imagesy($source_image);
            
        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefromjpeg($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefrompng($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($width < 1)                                      {   return 1;   }


        /* find the "desired height" of this thumbnail, relative to the desired width  */

        $desired_height                                     =   floor($height * ($desired_width / $width));

        /* create a new, "virtual" image */

        $virtual_image                                      =   imagecreatetruecolor($desired_width, $desired_height);

        /* copy source image at a resized size */

        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

        /* create the physical thumbnail image to its destination */

        imagejpeg($virtual_image);

        
    }
}	
