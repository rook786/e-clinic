<?php
namespace App\Traits;
use App;

trait response
{
    protected function response($name)
    {
        return $name;
    }

	

	public function kSuccess($message, $data='', $extra='')
	{

		$response['status_code']    =  1;
        $response['status_text']    =  'Success';
        $response['message']        =  $message;
		$response['data']           =  $data;

		if($extra != '' || $extra != null)
		{
			foreach($extra as $key => $value)
			{
				$response[$key] = $value;
			}
		}
		
		return $response;

	}


	public function kFailed($message, $extra='')
	{

		$response['status_code']    =  0;
        $response['status_text']    =  'Failed';
        $response['message']        =  $message;
		$response['data']           =  [];

		if($extra != '' || $extra != null)
		{
			foreach($extra as $key => $value)
			{
				$response[$key] = $value;
			}
		}
		
		return $response;

	}


	public function kAccessDenied()
	{

		$response['status_code']    =  5;
        $response['status_text']    =  'Failed';
        $response['message']        =  'Access Denied';
		$response['data']           =  [];
		return $response;

	}


	public function kUserSuccess($message, $data='', $extra='')
	{

		$response['status_code']    =  1;
        $response['status_text']    =  'Success';
        $response['message']        =  $message;
		$response['user_data']      =  $data;

		if($extra != '' || $extra != null)
		{
			foreach($extra as $key => $value)
			{
				$response[$key] 	= 	$value;
			}
		}
		
		return $response;

	}

	public function kUserFailed($message, $extra='')
	{

		$response['status_code']    =  0;
        $response['status_text']    =  'Failed';
        $response['message']        =  $message;
		$response['user_data']      =  [];

		if($extra != '' || $extra != null)
		{
			foreach($extra as $key => $value)
			{
				$response[$key] 	= 	$value;
			}
		}
		
		return $response;

	}


	public function kAdminBlock()
	{

		$response['status_code']    =  	3;
        $response['status_text']    =  	'Failed';
        $response['message']        =  	'Your account is blocked by the admin';
		$response['user_data']      =  	[];
		return $response;

	}


	public function kCredentials()
	{

		$response['status_code']    =  	4;
        $response['status_text']    =  	'Failed';
        $response['message']        =  	'Please enter correct username and password';
		$response['user_data']      =  	[];
		return $response;

	}


	public function kNotFound()
	{

		$response['status_code']    =  	0;
        $response['status_text']    =  	'user_not_found';
        $response['message']        =  	'User not found';
		$response['user_data']      =  	[];
		return $response;

	}


	public function kViewProfile()
	{

		$response['status_code']    =  	6;
        $response['status_text']    =  	'Failed';
        $response['message']        =  	'You are not allowed to view this profile';
		$response['user_data']      =  	[];
		return $response;

	}


	public function kTwoFa($user_id)
	{

		$response['status_code']    =  	2;
        $response['status_text']    =  	'Failed';
		$response['message']        =  	'2FA status ON';
		$response['user_id']		=	$user_id;
		$response['user_data']      =  	[];
		return $response;

	}


	public function kPendingRequest()
	{

		$response['status_code']    =  	2;
        $response['status_text']    =  	'Failed';
        $response['message']        =  	'Please clear your Pending Requests before turning back your profile to Public';
		$response['user_data']      =  	[];
		return $response;

	}



	
	
}	
