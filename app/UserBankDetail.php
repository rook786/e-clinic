<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBankDetail extends Model
{
    protected $fillable = ['user_id','bank_name','account_name','account_no','ifsc_code','branch'];
}
