<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitingOpdDetail extends Model
{
    protected $fillable = ['opd_type','week_day','visiting_staff_id','open_time','close_time','venue','status'];
}
