<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftType extends Model
{
    protected $fillable = ['title','identifier','status'];
}
