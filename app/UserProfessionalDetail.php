<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfessionalDetail extends Model
{
    protected $fillable = ['user_id','hospital_name','address','date_of_joining','date_of_relieving','total_experience','reason_of_relieving'];
}
