<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitingStaff extends Model
{
    protected $fillable = ['department','user_type','first_name','last_name','email','password','mobile','profile_image','profile_image_thumb','hospital_name','address','status'];
}
