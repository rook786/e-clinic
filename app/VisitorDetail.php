<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorDetail extends Model
{
    protected $fillable = ['date','time','visitor_type','first_name','last_name','email','mobile','visiting_reason','organization_name','address','designation','concerned_person','department'];
}
