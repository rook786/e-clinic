<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEducationDetail extends Model
{
    protected $fillable = ['user_id','course','university','enrollment_no','year_of_passing','marks_obtained','total_marks','division','grade'];
}
