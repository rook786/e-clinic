<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WardDetail extends Model
{
    protected $fillable = ['ward_name','ward_location','description','capacity','status'];
}
