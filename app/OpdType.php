<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpdType extends Model
{
    protected $fillable = ['title','identifier','status'];
}
