<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftDetail extends Model
{
    protected $fillable = ['date','shift_type','department','description','shift_start_time','shift_end_time','break_start_time','break_end_time','venue','users'];
}
