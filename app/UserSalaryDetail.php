<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSalaryDetail extends Model
{
    protected $fillable = ['user_id','basic_salary','dearness_allowance','house_rent_allowance','leave_travel_allowance','conveyance_allowance','medical_allowance','education_allowance','hostel_allowance'];
}
