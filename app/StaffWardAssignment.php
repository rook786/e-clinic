<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffWardAssignment extends Model
{
    protected $fillable = ['staff_id','ward','from_date','to_date'];
}
