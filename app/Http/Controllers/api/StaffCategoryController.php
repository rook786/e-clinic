<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class StaffCategoryController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 27.1  Create Staff Category  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:staff_categories,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Staff Category Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $staff_category = \App\StaffCategory::create($input);

        if($staff_category == '')                                {   return $this->kFailed('Unable To Create Staff Category');  }

        return $this->kSuccess('Staff Category Created Successfully'); 
    }


    
    //*************************** Route No. 27.2  Get Staff Category List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\StaffCategory;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Staff Category Found');   }
                
        return $this->kSuccess('Staff Category List Fetched Successfully',$result);
    }


    //*************************** Route No. 27.3  Update Staff Category  ********************************


    public function update(Request $request, $id)
    {
         
        $staff_category = \App\StaffCategory::Find($id);

        if(!$staff_category) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:staff_categories,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Staff Category Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $staff_category = $staff_category->update($input);

        return $this->kSuccess('Staff Category Updated Successfully');
    }


    //*************************** Route No. 1274  Delete Staff Category  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $staff_category = \App\StaffCategory::Find($id);

        if(!$staff_category) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Staff Category ****************

        // $staff_category_usage = \App\UserType::where('staff_category',$id)->count();

        // if($staff_category_usage > 0)    {  return $this->kFailed('Staff Category Assigned to User Type'); }

        
        // *********** Delete data from staff_categories table ****************

        $staff_category->where('id',$id)->delete();

        return $this->kSuccess('Staff Category Deleted Successfully');
    }


    //*************************** Route No. 27.5  Update Staff Category Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $staff_category = \App\StaffCategory::Find($id);

        if(!$staff_category) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $staff_category_status = $staff_category->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\StaffCategory::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
