<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class UserDocumentTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 3.1  Create User Document Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:user_document_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'User Document Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $user_document_type = \App\UserDocumentType::create($input);

        if($user_document_type == '')                                {   return $this->kFailed('Unable To Create User Document Type');  }

        return $this->kSuccess('User Document Type Created Successfully'); 
    }


    
    //*************************** Route No. 3.2  Get User Document Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\UserDocumentType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No User Document Type Found');   }
                
        return $this->kSuccess('User Document Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 3.3  Update User Document Type  ********************************


    public function update(Request $request, $id)
    {
         
        $user_document_type = \App\UserDocumentType::Find($id);

        if(!$user_document_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:user_document_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'User Document Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $user_document_type = $user_document_type->update($input);

        return $this->kSuccess('User Document Type Updated Successfully');
    }


    //*************************** Route No. 3.4  Delete User Document Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $user_document_type = \App\UserDocumentType::Find($id);

        if(!$user_document_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of User Document Type ****************

        // $user_document_type_usage = \App\UserDocument::where('user_document_type',$id)->count();

        // if($user_document_type_usage > 0)    {  return $this->kFailed('User Document Type Assigned to User'); }

        
        // *********** Delete data from user_types table ****************

        $user_document_type->where('id',$id)->delete();

        return $this->kSuccess('User Document Type Deleted Successfully');
    }


    //*************************** Route No. 3.5  Update User Document Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $user_document_type = \App\UserDocumentType::Find($id);

        if(!$user_document_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $user_document_type_status = $user_document_type->update([ 'status' => $request->status ]);

        $result = \App\UserDocumentType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
