<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class ShiftTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 12.1  Create Shift Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:shift_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Shift Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $shift_type = \App\ShiftType::create($input);

        if($shift_type == '')                                {   return $this->kFailed('Unable To Create Shift Type');  }

        return $this->kSuccess('Shift Type Created Successfully'); 
    }


    
    //*************************** Route No. 12.2  Get Shift Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\ShiftType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Shift Type Found');   }
                
        return $this->kSuccess('Shift Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 12.3  Update Shift Type  ********************************


    public function update(Request $request, $id)
    {
         
        $shift_type = \App\ShiftType::Find($id);

        if(!$shift_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:shift_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Shift Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $shift_type = $shift_type->update($input);

        return $this->kSuccess('Shift Type Updated Successfully');
    }


    //*************************** Route No. 12.4  Delete Shift Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $shift_type = \App\ShiftType::Find($id);

        if(!$shift_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Shift Type ****************

        // $shift_type_usage = \App\ShiftDetail::where('shift_type',$id)->count();

        // if($shift_type_usage > 0)    {  return $this->kFailed('Shift Type Assigned to Shift'); }

        
        // *********** Delete data from shift_types table ****************

        $shift_type->where('id',$id)->delete();

        return $this->kSuccess('Shift Type Deleted Successfully');
    }


    //*************************** Route No. 12.5  Update Shift Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $shift_type = \App\ShiftType::Find($id);

        if(!$shift_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $shift_type_status = $shift_type->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\ShiftType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
