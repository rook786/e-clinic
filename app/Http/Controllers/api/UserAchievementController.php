<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
//use App\Traits\image;
use Validator;
use App;
use File;


class UserAchievementController extends Controller
{
    use trait_functions, response;

    
    //*************************** Route No. 16.1  Create User Achievement  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'achievement_category'  => 'required|exists:achievement_categories,id',
            'achievement_type'      => 'required|exists:achievement_types,id',
            'achievement_name'      => 'required|string|min:2',
            'description'           => 'sometimes|min:10',
            'year'                  => 'required|string|min:4|max:4',
            'image'                 => 'required|string',
        ],
        [
            'user_id.exists'                => 'User does not exist',
            'achievement_category.exists'   => 'Invalid Achievement Category',
            'achievement_type.exists'       => 'Invalid Achievement Type',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $user_achievement = \App\UserAchievement::create($input);

        if($user_achievement == '')                                {   return $this->kFailed('Unable To Create User Achievement');  }

        return $this->kSuccess('User Achievement Created Successfully'); 
    }


    
    //*************************** Route No. 16.2  List of User Achievements  ********************************



    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_id            =   $this->validate_var(@$_GET['user_id'], '');
       
               
        $model      =   new App\UserAchievement;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            
            $new_result->achievement_type = \App\AchievementType::where('id',$new_result->achievement_type)->first()->title;
            $new_result->achievement_category = \App\AchievementCategory::where('id',$new_result->achievement_category)->first()->title;
                  
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No User Achievement Found');   }
                
        return $this->kSuccess('Achievement List Fetched Successfully',$result);
    }


    //*************************** Route No. 16.3  Update User Achievement  ********************************


    public function update(Request $request, $id)
    {
         
        $user_achievement = \App\UserAchievement::Find($id);

        if(!$user_achievement) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'achievement_category'  => 'required|exists:achievement_categories,id',
            'achievement_type'      => 'required|exists:achievement_types,id',
            'achievement_name'      => 'required|string|min:2',
            'description'           => 'sometimes|min:10',
            'year'                  => 'required|string|min:4|max:4',
            'image'                 => 'required|string',
        ],
        [
            'user_id.exists'                => 'User does not exist',
            'achievement_category.exists'   => 'Invalid Achievement Category',
            'achievement_type.exists'       => 'Invalid Achievement Type',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $user_achievement = $user_achievement->update($input);

        return $this->kSuccess('User Achievement Updated Successfully');
    }


    //*************************** Route No. 16.4  Delete User Achievement  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $user_achievement = \App\UserAchievement::Find($id);

        if(!$user_achievement) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from user_achievements table ****************

        $user_achievement->where('id',$id)->delete();

        return $this->kSuccess('User Achievement Deleted Successfully');
    }



    //*************************** Route No. 16.5  Upload Achievement Image  ********************************
    
    
    public function upload_achievement_image(Request $request)
    {   
        $validator  =   Validator::make($request->all(), [

            'user_id' => 'required|exists:users,id',
            'image'   => 'required|mimes:jpeg,jpg,png'
        ]);

        if ($validator->errors()->all())                                {   return $this->kFailed($validator->errors()->first());   } 
                
        $path                                                           =   public_path().'/images/users/'.$request->user_id;

        if(!File::exists($path))                                        {   File::makeDirectory($path, $mode = 0777, true, true);   }
        
        if(isset($request->image) && !empty($request->image))
        {
            $unique_string                                              =   'achievement-'.strtotime(date('Y-m-d h:i:s')); 

            $file                                                       =   $request->image;   
            
            $photo_name                                                 =   $unique_string.$file->getClientOriginalName();
                                    
            $thumb_name_main                                            =   $photo_name;

            $thumb_name = "thumb_".$photo_name;

            $file->move($path,$photo_name);

            $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'200');
            
        }
        else                                                            {   $photo_name = '';   }       
        
        $extra                                                          =   array();
        $extra['data'][]["image_url"]                                   =   $request->user_id.'/'.$thumb_name_main;
       
        return $this->kSuccess('Image is Uploaded','',$extra);
    }



    public function make_thumb($src, $dest, $desired_width)
    {

        /* read the source image */

        $width                                               =  @imagesx($source_image);
        $height                                              =  @imagesy($source_image);
            
        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefromjpeg($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefrompng($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($width < 1)                                      {   return 1;   }


        /* find the "desired height" of this thumbnail, relative to the desired width  */

        $desired_height                                     =   floor($height * ($desired_width / $width));

        /* create a new, "virtual" image */

        $virtual_image                                      =   imagecreatetruecolor($desired_width, $desired_height);

        /* copy source image at a resized size */

        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

        /* create the physical thumbnail image to its destination */

        imagejpeg($virtual_image, $dest);

        
    }


}
