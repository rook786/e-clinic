<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;
use File;


class UserDocumentController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 21.1  Create User Document  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'document_type'         => 'required|exists:user_document_types,id',
            'document_path'         => 'required|string',
        ],
        [
            'document_type.exists'   => 'Invalid Document Type',
            'user_id.exists'         => 'User Does not Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $file                                                   =   $input['document_path'];                       
        $arr                                                    =   explode('/',$file);
        $arr1                                                   =   $arr[0];
        $arr2                                                   =   $arr[1];
        $final_thumb_name                                       =   $arr1."/thumb_".$arr2;

        $input['document_thumb_path'] = $final_thumb_name;

        $user_document = \App\UserDocument::create($input);

        if($user_document == '')                                {   return $this->kFailed('Unable To Create User Document');  }

        return $this->kSuccess('User Document Created Successfully'); 
    }


    
    //*************************** Route No. 21.2  List User Documents  ********************************



    public function get_list()
    {

        $per_page       =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby        =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order          =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_id        =   $this->validate_var(@$_GET['user_id'], '');
        $document_type   =   $this->validate_var(@$_GET['document_type'], '');
        
               
        $model      =   new App\UserDocument;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }

        if($document_type != '' || $document_type != null)
        {   
            $model = $model->where('document_type' , $document_type);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->document_type = \App\UserDocumentType::where('id',$new_result->document_type)->first()->title;
            $new_result->person_name = \App\User::where('id',$new_result->user_id)->first()->first_name;
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Vehicle Detail Found');   }
                
        return $this->kSuccess('Vehicle List Fetched Successfully',$result);
    }


    //*************************** Route No. 21.3  Update User Document  ********************************


    public function update(Request $request, $id)
    {
         
        $user_document = \App\UserDocument::Find($id);

        if(!$user_document) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'document_type'         => 'required|exists:user_document_types,id',
            'document_path'         => 'required|string'
        ],
        [
            'document_type.exists'   => 'Invalid Document Type',
            'user_id.exists'         => 'User Does not Exist'
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        if($input['document_path'] != '')
        {
            $file                                                   =   $input['document_path'];                       
            $arr                                                    =   explode('/',$file);
            $arr1                                                   =   $arr[0];
            $arr2                                                   =   $arr[1];
            $final_thumb_name                                       =   $arr1."/thumb_".$arr2;

            $input['document_thumb_path'] = $final_thumb_name;
        }

        $user_document = $user_document->update($input);

        return $this->kSuccess('User Document Updated Successfully');
    }


    //*************************** Route No. 21.4  Delete User Document  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $user_document = \App\UserDocument::Find($id);

        if(!$user_document) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from user_documents table ****************

        $user_document->where('id',$id)->delete();

        return $this->kSuccess('User Document Deleted Successfully');
    }


    //*************************** Route No. 21.5  Upload Document Image  ********************************
    
    
    public function upload_document_image(Request $request)
    {   
        $validator  =   Validator::make($request->all(), [

            'user_id'           => 'required|exists:users,id',
            'document_image'    => 'required|mimes:jpeg,jpg,png'
        ]);

        if ($validator->errors()->all())                                {   return $this->kFailed($validator->errors()->first());   } 
                
        $path                                                           =   public_path().'/images/users/'.$request->user_id;

        if(!File::exists($path))                                        {   File::makeDirectory($path, $mode = 0777, true, true);   }
        
        if(isset($request->document_image) && !empty($request->document_image))
        {
            $unique_string                                              =   'document-'.strtotime(date('Y-m-d h:i:s')); 

            $file                                                       =   $request->document_image;   
            
            $photo_name                                                 =   $unique_string.$file->getClientOriginalName();
                                    
            $thumb_name_main                                            =   $photo_name;

            $thumb_name = "thumb_".$photo_name;

            $file->move($path,$photo_name);

            $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'400');
            
        }
        else                                                            {   $photo_name = '';   }       
        
        $extra                                                          =   array();
        $extra['data'][]["image_url"]                                   =   $request->user_id.'/'.$thumb_name_main;
       
        return $this->kSuccess('Document is Uploaded','',$extra);
    }



    public function make_thumb($src, $dest, $desired_width)
    {

        /* read the source image */

        $width                                               =  @imagesx($source_image);
        $height                                              =  @imagesy($source_image);
            
        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefromjpeg($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefrompng($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($width < 1)                                      {   return 1;   }


        /* find the "desired height" of this thumbnail, relative to the desired width  */

        $desired_height                                     =   floor($height * ($desired_width / $width));

        /* create a new, "virtual" image */

        $virtual_image                                      =   imagecreatetruecolor($desired_width, $desired_height);

        /* copy source image at a resized size */

        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

        /* create the physical thumbnail image to its destination */

        imagejpeg($virtual_image, $dest);

        
    }
}
