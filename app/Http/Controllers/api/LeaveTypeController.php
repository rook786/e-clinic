<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class LeaveTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 7.1  Create Leave Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:leave_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Leave Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $leave_type = \App\LeaveType::create($input);

        if($leave_type == '')                                {   return $this->kFailed('Unable To Create Leave Type');  }

        return $this->kSuccess('Leave Type Created Successfully'); 
    }


    
    //*************************** Route No. 7.2  Get Leave Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\LeaveType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Leave Type Found');   }
                
        return $this->kSuccess('Leave Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 7.3  Update Leave Type  ********************************


    public function update(Request $request, $id)
    {
         
        $leave_type = \App\LeaveType::Find($id);

        if(!$leave_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:leave_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Leave Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $leave_type = $leave_type->update($input);

        return $this->kSuccess('Leave Type Updated Successfully');
    }


    //*************************** Route No. 7.4  Delete Leave Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $leave_type = \App\LeaveType::Find($id);

        if(!$leave_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Leave Type ****************

        // $leave_type_usage = \App\UserLeave::where('leave_type',$id)->count();

        // if($leave_type_usage > 0)    {  return $this->kFailed('Leave Type Assigned to User'); }

        
        // *********** Delete data from leave_types table ****************

        $leave_type->where('id',$id)->delete();

        return $this->kSuccess('Leave Type Deleted Successfully');
    }


    //*************************** Route No. 7.5  Update Leave Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $leave_type = \App\LeaveType::Find($id);

        if(!$leave_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $leave_type_status = $leave_type->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\LeaveType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
