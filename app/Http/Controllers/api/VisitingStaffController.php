<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;
use Hash;


class VisitingStaffController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 20.1  Create Visiting Staff  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'department'            => 'required|exists:departments,id',
            'user_type'             => 'required|exists:user_types,id',
            'first_name'            => 'required|string|min:2',
            'email'                 => 'required|email|unique:visiting_staff,email',
            'password'              => 'required|string',
            'mobile'                => 'required|unique:visiting_staff,mobile|max:12',
            'hospital_name'         => 'required',
            'address'               => 'required',
        ],
        [
            'department.exists'     => 'Invalid Department',
            'user_type.exists'      => 'Invalid User Type',
            'email.unique'          => 'Email Already Exist',
            'mobile.unique'         => 'Mobile Number Already Exist',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        $visiting_staff = \App\VisitingStaff::create($input);

        if($visiting_staff == '')                                {   return $this->kFailed('Unable To Create Visiting Staff');  }

        return $this->kSuccess('Visiting Staff Created Successfully'); 
    }


    
    //*************************** Route No. 20.2  Get Visiting Staff List  ********************************



    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_type          =   $this->validate_var(@$_GET['user_type'], '');
        $department         =   $this->validate_var(@$_GET['department'], '');
        $user_id            =   $this->validate_var(@$_GET['user_id'], '');
               
        $model      =   new App\VisitingStaff;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('id' , $user_id);  
        }

        if($user_type != '' || $user_type != null)
        {   
            $model = $model->where('user_type' , $user_type);  
        }

        if($department != '' || $department != null)
        {   
            $model = $model->where('department' , $department);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->user_type = \App\UserType::where('id',$new_result->user_type)->first()->title;
            $new_result->department = \App\Department::where('id',$new_result->department)->first()->title;
            
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Visiting Staff Found');   }
                
        return $this->kSuccess('Visiting Staff List Fetched Successfully',$result);
    }


    //*************************** Route No. 20.3  Update Visiting Staff  ********************************


    public function update(Request $request, $id)
    {
         
        $visiting_staff = \App\VisitingStaff::Find($id);

        if(!$visiting_staff) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'department'            => 'required|exists:departments,id',
            'user_type'             => 'required|exists:user_types,id',
            'first_name'            => 'required|string|min:2',
            'email'                 => 'required|email|unique:visiting_staff,email,'.$id,
            'mobile'                => 'required|unique:visiting_staff,mobile,'.$id.'|max:12',
            'hospital_name'         => 'required',
            'address'               => 'required',
        ],
        [
            'department.exists'     => 'Invalid Department',
            'user_type.exists'      => 'Invalid User Type',
            'email.unique'          => 'Email Already Exist',
            'mobile.unique'         => 'Mobile Number Already Exist',
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $visiting_staff = $visiting_staff->update($input);

        return $this->kSuccess('Visiting Staff Updated Successfully');
    }


    //*************************** Route No. 20.4  Delete Visiting Staff  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $visiting_staff = \App\VisitingStaff::Find($id);

        if(!$visiting_staff) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from visiting_staff table ****************

        $visiting_staff->where('id',$id)->delete();

        return $this->kSuccess('Visiting Staff Deleted Successfully');
    }



    //*************************** Route No. 20.5  Update Visiting Staff Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $visiting_staff = \App\VisitingStaff::Find($id);

        if(!$visiting_staff) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $visiting_staff_status = $visiting_staff->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\VisitingStaff::where('id',$id)->get();


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }

}
