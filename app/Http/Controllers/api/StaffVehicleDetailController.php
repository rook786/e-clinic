<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class StaffVehicleDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 14.1  Create Staff Vehicle Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'vehicle_type'          => 'required|exists:vehicle_types,id',
            'company_name'          => 'required|string|min:2',
            'model'                 => 'required|string|min:2',
            'color'                 => 'required|string|min:2',
            'registration_no'       => 'required|string'
        ],
        [
            'vehicle_type.exists'   => 'Invalid Vehicle Type',
            'user_id.exists'        => 'User Does not Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $staff_vehicle_detail = \App\StaffVehicleDetail::create($input);

        if($staff_vehicle_detail == '')                                {   return $this->kFailed('Unable To Create Staff Vehicle Detail');  }

        return $this->kSuccess('Staff Vehicle Detail Created Successfully'); 
    }


    
    //*************************** Route No. 14.2  Get Vehicle List  ********************************



    public function get_list()
    {

        $per_page       =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby        =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order          =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_id        =   $this->validate_var(@$_GET['user_id'], '');
        $vehicle_type   =   $this->validate_var(@$_GET['vehicle_type'], '');
        
               
        $model      =   new App\StaffVehicleDetail;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }

        if($vehicle_type != '' || $vehicle_type != null)
        {   
            $model = $model->where('vehicle_type' , $vehicle_type);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->vehicle_type = \App\VehicleType::where('id',$new_result->vehicle_type)->first()->title;
            $new_result->person_name = \App\User::where('id',$new_result->user_id)->first()->first_name;
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Vehicle Detail Found');   }
                
        return $this->kSuccess('Vehicle List Fetched Successfully',$result);
    }


    //*************************** Route No. 14.3  Update Staff Vehicle Detail  ********************************


    public function update(Request $request, $id)
    {
         
        $staff_vehicle_detail = \App\StaffVehicleDetail::Find($id);

        if(!$staff_vehicle_detail) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'vehicle_type'          => 'required|exists:vehicle_types,id',
            'company_name'          => 'required|string|min:2',
            'model'                 => 'required|string|min:2',
            'color'                 => 'required|string|min:2',
            'registration_no'       => 'required|string'
        ],
        [
            'vehicle_type.exists'   => 'Invalid Vehicle Type',
            'user_id.exists'        => 'Staff Does not Exist'
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $staff_vehicle_detail = $staff_vehicle_detail->update($input);

        return $this->kSuccess('Staff Vehicle Detail Updated Successfully');
    }


    //*************************** Route No. 14.4  Delete Staff Vehicle Detail  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $staff_vehicle_detail = \App\StaffVehicleDetail::Find($id);

        if(!$staff_vehicle_detail) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from visitor_types table ****************

        $staff_vehicle_detail->where('id',$id)->delete();

        return $this->kSuccess('Staff Vehicle Detail Deleted Successfully');
    }

}
