<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class AchievementCategoryController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 10.1  Create Achievement Category  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:achievement_categories,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Achievement Category Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $achievement_category = \App\AchievementCategory::create($input);

        if($achievement_category == '')                                {   return $this->kFailed('Unable To Create Achievement Category');  }

        return $this->kSuccess('Achievement Category Created Successfully'); 
    }


    
    //*************************** Route No. 10.2  Get Achievement Category List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\AchievementCategory;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Achievement Category Found');   }
                
        return $this->kSuccess('Achievement Category List Fetched Successfully',$result);
    }


    //*************************** Route No. 10.3  Update Achievement Category  ********************************


    public function update(Request $request, $id)
    {
         
        $achievement_category = \App\AchievementCategory::Find($id);

        if(!$achievement_category) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:achievement_categories,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Achievement Category Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $achievement_category = $achievement_category->update($input);

        return $this->kSuccess('Achievement Category Updated Successfully');
    }


    //*************************** Route No. 10.4  Delete Achievement Category  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $achievement_category = \App\AchievementCategory::Find($id);

        if(!$achievement_category) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Achievement Category ****************

        // $achievement_category_usage = \App\UserAchievement::where('achievement_category_type',$id)->count();

        // if($achievement_category_usage > 0)    {  return $this->kFailed('Achievement Category Assigned to User'); }

        
        // *********** Delete data from achievement_categories table ****************

        $achievement_category->where('id',$id)->delete();

        return $this->kSuccess('Achievement Category Deleted Successfully');
    }


    //*************************** Route No. 10.5  Update Achievement Category Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $achievement_category = \App\AchievementCategory::Find($id);

        if(!$achievement_category) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $achievement_category_status = $achievement_category->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\AchievementCategory::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
