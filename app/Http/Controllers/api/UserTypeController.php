<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class UserTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 1.1  Create User Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'staff_category'    => 'required|exists:staff_categories,id',
            'title'             => 'required|string|unique:user_types,title|string|min:2'
            
        ],
        [
            'staff_category.exists' => 'Invalid Staff Categories',
            'title.unique' => 'User Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $user_type = \App\UserType::create($input);

        if($user_type == '')                                {   return $this->kFailed('Unable To Create User Type');  }

        return $this->kSuccess('User Type Created Successfully'); 
    }


    
    //*************************** Route No. 1.2  Get User Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
        $staff_category     =   $this->validate_var(@$_GET['staff_category'], '');
               
        $model      =   new App\UserType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        if($staff_category != '' || $staff_category != null)
        {   
            $model = $model->where('staff_category' , $staff_category);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','staff_category','title','identifier','status'])->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->staff_category = \App\StaffCategory::where('id',$new_result->staff_category)->first()->title;
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No User Type Found');   }
                
        return $this->kSuccess('User Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 1.3  Update User Type  ********************************


    public function update(Request $request, $id)
    {
         
        $user_type = \App\UserType::Find($id);

        if(!$user_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'staff_category'    => 'required|exists:staff_categories,id',
            'title'             => 'required|string|unique:user_types,title|string|min:2'
            
        ],
        [
            'staff_category.exists' => 'Invalid Staff Categories',
            'title.unique' => 'User Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $user_type = $user_type->update($input);

        return $this->kSuccess('User Type Updated Successfully');
    }


    //*************************** Route No. 1.4  Delete User Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $user_type = \App\UserType::Find($id);

        if(!$user_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of user type ****************

        // $user_type_usage = \App\User::where('user_type',$id)->count();

        // if($user_type_usage > 0)    {  return $this->kFailed('User Type Assigned to User'); }

        
        // *********** Delete data from user_types table ****************

        $user_type->where('id',$id)->delete();

        return $this->kSuccess('User Type Deleted Successfully');
    }


    //*************************** Route No. 1.5  Update User Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $user_type = \App\UserType::Find($id);

        if(!$user_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $user_type_status = $user_type->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\UserType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
