<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class OpdDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 18.1  Create Opd Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'opd_type'              => 'required|exists:opd_types,id',
            'week_day'              => 'required|exists:week_days,id',
            'user_id'               => 'required|exists:users,id',
            'open_time'             => 'required|date_format:H:i',
            'close_time'            => 'required|date_format:H:i',
            'break_start_time'      => 'required|date_format:H:i',
            'break_end_time'        => 'required|date_format:H:i',
            'venue'                 => 'required|string',
        ],
        [
            'opd_type.exists'               => 'Invalid Opd Type',
            'week_day.exists'               => 'Invalid Week Day',
            'user_is.exists'                => 'User does not Exist',
            'open_time.date_format'         => 'Invalid Open Time Format',
            'close_time.date_format'        => 'Invalid Open Time Format',
            'break_start_time.date_format'  => 'Invalid Open Time Format',
            'break_end_time.date_format'    => 'Invalid Open Time Format',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $opd_detail = \App\OpdDetail::create($input);

        if($opd_detail == '')                                {   return $this->kFailed('Unable To Create Opd Detail');  }

        return $this->kSuccess('Opd Detail Created Successfully'); 
    }


    
    //*************************** Route No. 18.2  List OPd Details  ********************************



    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $opd_type           =   $this->validate_var(@$_GET['opd_type'], '');
        $week_day           =   $this->validate_var(@$_GET['week_day'], '');
        $user_id            =   $this->validate_var(@$_GET['user_id'], '');
               
        $model      =   new App\OpdDetail;
              
        if($opd_type != '' || $opd_type != null)
        {   
            $model = $model->where('opd_type' , $opd_type);  
        }

        if($week_day != '' || $week_day != null)
        {   
            $model = $model->where('week_day' , $week_day);  
        }

        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->opd_type = \App\OpdType::where('id',$new_result->opd_type)->first()->title;
            $new_result->week_day = \App\WeekDay::where('id',$new_result->week_day)->first()->title;
            $new_result->username = \App\User::where('id',$new_result->user_id)->first()->first_name;
            $new_result->formatted_open_time = date('h:i A',strtotime($new_result->open_time));
            $new_result->formatted_close_time = date('h:i A',strtotime($new_result->close_time));
            $new_result->formatted_break_start_time = date('h:i A',strtotime($new_result->break_start_time));
            $new_result->formatted_break_end_time = date('h:i A',strtotime($new_result->break_end_time));
            
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Opd Detail Found');   }
                
        return $this->kSuccess('Visitor List Fetched Successfully',$result);
    }


    //*************************** Route No. 18.3  Update Opd Detail  ********************************


    public function update(Request $request, $id)
    {
         
        $opd_detail = \App\OpdDetail::Find($id);

        if(!$opd_detail) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'opd_type'              => 'required|exists:opd_types,id',
            'week_day'              => 'required|exists:week_days,id',
            'user_id'               => 'required|exists:users,id',
            'open_time'             => 'required|date_format:H:i',
            'close_time'            => 'required|date_format:H:i',
            'break_start_time'      => 'required|date_format:H:i',
            'break_end_time'        => 'required|date_format:H:i',
            'venue'                 => 'required|string',
        ],
        [
            'opd_type.exists'               => 'Invalid Opd Type',
            'week_day.exists'               => 'Invalid Week Day',
            'user_is.exists'                => 'User does not Exist',
            'open_time.date_format'         => 'Invalid Open Time Format',
            'close_time.date_format'        => 'Invalid Open Time Format',
            'break_start_time.date_format'  => 'Invalid Open Time Format',
            'break_end_time.date_format'    => 'Invalid Open Time Format',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $opd_detail = $opd_detail->update($input);

        return $this->kSuccess('Opd Detail Updated Successfully');
    }


    //*************************** Route No. 18.4  Delete Opd Detail  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $opd_detail = \App\OpdDetail::Find($id);

        if(!$opd_detail) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from opd_details table ****************

        $opd_detail->where('id',$id)->delete();

        return $this->kSuccess('Opd Detail Deleted Successfully');
    }



    //*************************** Route No. 18.5  Update Opd Detail Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $opd_detail = \App\OpdDetail::Find($id);

        if(!$opd_detail) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $opd_detail_status = $opd_detail->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\OpdDetail::where('id',$id)->get();


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }

}
