<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;
use Hash;


class WardDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 28.1  Create Ward Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'ward_name'           => 'required|string',
            'ward_location'       => 'required|string',
            'description'         => 'sometimes|string',
            'capacity'            => 'required',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $ward_deatil = \App\WardDetail::create($input);

        if($ward_deatil == '')                                {   return $this->kFailed('Unable To Create Ward Detail');  }

        return $this->kSuccess('Ward Detail Created Successfully'); 
    }


    
    //*************************** Route No. 28.2  Get Ward Detail List  ********************************



    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $status             =   $this->validate_var(@$_GET['status'], '');

        $model      =   new App\WardDetail; 

        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }
       
        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Ward Detail Found');   }
                
        return $this->kSuccess('Ward Detail List Fetched Successfully',$result);
    }


    //*************************** Route No. 28.3  Update Ward Detail  ********************************


    public function update(Request $request, $id)
    {
         
        $ward_deatil = \App\WardDetail::Find($id);

        if(!$ward_deatil) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'ward_name'           => 'required|string',
            'ward_location'       => 'required|string',
            'description'         => 'sometimes|string',
            'capacity'            => 'required',
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $ward_deatil = $ward_deatil->update($input);

        return $this->kSuccess('Ward Detail Updated Successfully');
    }


    //*************************** Route No. 28.4  Delete Ward Detail  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $ward_deatil = \App\WardDetail::Find($id);

        if(!$ward_deatil) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from ward_details table ****************

        $ward_deatil->where('id',$id)->delete();

        return $this->kSuccess('Ward Detail Deleted Successfully');
    }



    //*************************** Route No. 28.5  Update Ward Detail Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $ward_deatil = \App\WardDetail::Find($id);

        if(!$ward_deatil) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $ward_deatil_status = $ward_deatil->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\WardDetail::where('id',$id)->get();


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }

}
