<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class VisitingOpdDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 19.1  Create Visiting Opd Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'opd_type'              => 'required|exists:opd_types,id',
            'week_day'              => 'required|exists:week_days,id',
            'visiting_staff_id'     => 'required|exists:visiting_staff,id',
            'open_time'             => 'required|date_format:H:i',
            'close_time'            => 'required|date_format:H:i',
            'venue'                 => 'required|string',
        ],
        [
            'opd_type.exists'               => 'Invalid Opd Type',
            'week_day.exists'               => 'Invalid Week Day',
            'visiting_staff_id.exists'      => 'Visiting Staff does not Exist',
            'open_time.date_format'         => 'Invalid Open Time Format',
            'close_time.date_format'        => 'Invalid Open Time Format',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $visiting_opd_detail = \App\VisitingOpdDetail::create($input);

        if($visiting_opd_detail == '')                                {   return $this->kFailed('Unable To Create Visiting Opd Detail');  }

        return $this->kSuccess('Visiting Opd Detail Created Successfully'); 
    }


    
    //*************************** Route No. 19.2  List Visiting Opd Details  ********************************



    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $opd_type           =   $this->validate_var(@$_GET['opd_type'], '');
        $week_day           =   $this->validate_var(@$_GET['week_day'], '');
        $user_id            =   $this->validate_var(@$_GET['user_id'], '');
               
        $model      =   new App\VisitingOpdDetail;
              
        if($opd_type != '' || $opd_type != null)
        {   
            $model = $model->where('opd_type' , $opd_type);  
        }

        if($week_day != '' || $week_day != null)
        {   
            $model = $model->where('week_day' , $week_day);  
        }

        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->opd_type = \App\OpdType::where('id',$new_result->opd_type)->first()->title;
            $new_result->week_day = \App\WeekDay::where('id',$new_result->week_day)->first()->title;
            $new_result->username = \App\VisitingStaff::where('id',$new_result->visiting_staff_id)->first()->first_name;
            $new_result->formatted_open_time = date('h:i A',strtotime($new_result->open_time));
            $new_result->formatted_close_time = date('h:i A',strtotime($new_result->close_time));
                      
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Visiting Opd Detail Found');   }
                
        return $this->kSuccess('Visitor List Fetched Successfully',$result);
    }


    //*************************** Route No. 19.3  Update Visiting Opd Detail  ********************************


    public function update(Request $request, $id)
    {
         
        $visiting_opd_detail = \App\VisitingOpdDetail::Find($id);

        if(!$visiting_opd_detail) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'opd_type'              => 'required|exists:opd_types,id',
            'week_day'              => 'required|exists:week_days,id',
            'visiting_staff_id'     => 'required|exists:visiting_staff,id',
            'open_time'             => 'required|date_format:H:i',
            'close_time'            => 'required|date_format:H:i',
            'venue'                 => 'required|string',
        ],
        [
            'opd_type.exists'               => 'Invalid Opd Type',
            'week_day.exists'               => 'Invalid Week Day',
            'visiting_staff_id.exists'      => 'Visiting Staff does not Exist',
            'open_time.date_format'         => 'Invalid Open Time Format',
            'close_time.date_format'        => 'Invalid Open Time Format',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $visiting_opd_detail = $visiting_opd_detail->update($input);

        return $this->kSuccess('Visiting Opd Detail Updated Successfully');
    }


    //*************************** Route No. 19.4  Delete Visiting Opd Detail  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $visiting_opd_detail = \App\VisitingOpdDetail::Find($id);

        if(!$visiting_opd_detail) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from visiting_opd_details table ****************

        $visiting_opd_detail->where('id',$id)->delete();

        return $this->kSuccess('Visiting Opd Detail Deleted Successfully');
    }



    //*************************** Route No. 19.5  Update Visiting Opd Detail Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $visiting_opd_detail = \App\VisitingOpdDetail::Find($id);

        if(!$visiting_opd_detail) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $opd_detail_status = $visiting_opd_detail->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\VisitingOpdDetail::where('id',$id)->get();


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }

}
