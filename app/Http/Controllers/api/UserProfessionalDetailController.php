<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class UserProfessionalDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 26.1  Create User Professional Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'   =>  'required|exists:users,id',
            'details'   =>  'required|array',
            'details.*.hospital_name'           => 'required|string',
            'details.*.address'                 => 'required|string',
            'details.*.date_of_joining'         => 'required|string|date_format:Y-m-d',
            'details.*.date_of_relieving'       => 'required|string|date_format:Y-m-d',
            'details.*.total_experience'        => 'required|string',
            'details.*.reason_of_relieving'     => 'required|string'
        ],
        [
            'user_id.exists'   => 'User Does not Exist',
            'details.*.date_of_joining.date_format' => 'Invalid Date of Joining Format',
            'details.*.date_of_relieving.date_format' => 'Invalid Date of Relieving Format'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $records_array = array();

        $details_array = $input['details'];

        foreach($details_array as $detail_array) 
        {
            $record = ['user_id' => $input['user_id'], 'hospital_name' => $detail_array['hospital_name'], 'address' => $detail_array['address'], 'date_of_joining' => $detail_array['date_of_joining'], 'date_of_relieving' => $detail_array['date_of_relieving'], 'total_experience' => $detail_array['total_experience'], 'reason_of_relieving' => $detail_array['reason_of_relieving']];

            $records_array[]    =   $record;

        }

        $user_professional_detail = \App\UserProfessionalDetail::insert($records_array);

        if($user_professional_detail == '')                                {   return $this->kFailed('Unable To Create User Professional Detail');  }

        return $this->kSuccess('User Professional Detail Created Successfully'); 
    }


    
    //*************************** Route No. 26.2   List User Professional Detail  ********************************


    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_id               =   $this->validate_var(@$_GET['user_id'], '');
       
               
        $model      =   new App\UserProfessionalDetail;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }
    
        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No User Professional Detail Found');   }
                
        return $this->kSuccess('User Professional Detail Fetched Successfully',$result);
    }




    //*************************** Route No. 26.3  Update User Professional Detail  ********************************



    public function update(Request $request, $id)
    {
         
        $user_professional_detail = \App\UserProfessionalDetail::Find($id);

        if(!$user_professional_detail) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'                 =>    'required|exists:users,id',
            'hospital_name'           =>    'required|string',
            'address'                 =>    'required|string',
            'date_of_joining'         =>    'required|string|date_format:Y-m-d',
            'date_of_relieving'       =>    'required|string|date_format:Y-m-d',
            'total_experience'        =>    'required|string',
            'reason_of_relieving'     =>    'required|string'
        ],
        [
            'user_id.exists'                  => 'User Does not Exist',
            'date_of_joining.date_format'     => 'Invalid Date of Joining Format',
            'date_of_relieving.date_format'   => 'Invalid Date of Relieving Format'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $user_professional_detail = $user_professional_detail->update($input);

        return $this->kSuccess('User Professional Detail Updated Successfully');
    }




    //*************************** Route No. 26.4  Delete User Professional Detail  ********************************

    
    
    public function destroy(Request $request, $id)
    {
        $user_professional_detail = \App\UserProfessionalDetail::Find($id);

        if(!$user_professional_detail) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from user_professional_details table ****************

        $user_professional_detail->where('id',$id)->delete();

        return $this->kSuccess('User Professional Detail Deleted Successfully');
    }

}
