<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class DepartmentController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 2.1  Create Department  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:departments,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Department Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $department = \App\Department::create($input);

        if($department == '')                                {   return $this->kFailed('Unable To Create Department');  }

        return $this->kSuccess('Department Created Successfully'); 
    }


    
    //*************************** Route No. 2.2  List All Departments  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\Department;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Department Found');   }
                
        return $this->kSuccess('Department List Fetched Successfully',$result);
    }


    //*************************** Route No. 2.3  Update Department  ********************************


    public function update(Request $request, $id)
    {
         
        $department = \App\Department::Find($id);

        if(!$department) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:departments,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Department Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $department = $department->update($input);

        return $this->kSuccess('Department Updated Successfully');
    }


    //*************************** Route No. 2.4  Delete Department  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $department = \App\Department::Find($id);

        if(!$department) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of department ****************

        // $department_usage = \App\User::where('department',$id)->count();

        // if($department_usage > 0)    {  return $this->kFailed('Department Assigned to User'); }

        
        // *********** Delete data from user_types table ****************

        $department->where('id',$id)->delete();

        return $this->kSuccess('Department Deleted Successfully');
    }


    //*************************** Route No. 2.5  Update Department Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $department = \App\Department::Find($id);

        if(!$department) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $department_status = $department->update([ 'status' => $request->status ]);

        $result = \App\Department::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
