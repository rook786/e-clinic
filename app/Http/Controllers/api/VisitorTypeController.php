<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class VisitorTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 9.1  Create Visitor Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:visitor_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Visitor Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $visitor_type = \App\VisitorType::create($input);

        if($visitor_type == '')                                {   return $this->kFailed('Unable To Create Visitor Type');  }

        return $this->kSuccess('Visitor Type Created Successfully'); 
    }


    
    //*************************** Route No. 9.2  Get Visitor Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\VisitorType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Visitor Type Found');   }
                
        return $this->kSuccess('Visitor Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 9.3  Update Visitor Type  ********************************


    public function update(Request $request, $id)
    {
         
        $visitor_type = \App\VisitorType::Find($id);

        if(!$visitor_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:visitor_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Visitor Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $visitor_type = $visitor_type->update($input);

        return $this->kSuccess('Visitor Type Updated Successfully');
    }


    //*************************** Route No. 9.4  Delete Visitor Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $visitor_type = \App\VisitorType::Find($id);

        if(!$visitor_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Visitor Type ****************

        // $visitor_type_usage = \App\VisitorDetail::where('visitor_type',$id)->count();

        // if($visitor_type_usage > 0)    {  return $this->kFailed('Visitor Type Assigned to Visitor'); }

        
        // *********** Delete data from visitor_types table ****************

        $visitor_type->where('id',$id)->delete();

        return $this->kSuccess('Visitor Type Deleted Successfully');
    }


    //*************************** Route No. 9.5  Update Visitor Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $visitor_type = \App\VisitorType::Find($id);

        if(!$visitor_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $visitor_type_status = $visitor_type->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\VisitorType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
