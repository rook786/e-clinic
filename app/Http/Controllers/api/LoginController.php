<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;
use File;
use Auth;
use Hash;
use DB;


class LoginController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 24.1  Login User  ********************************


    public function login(Request $request)
    {
    	
    	// *********** Check for required fields ****************

            $validator=Validator::make($request->all(), [

               	'email' 	    =>  'required|email|exists:users,email',
                'password' 	    =>  'required|string',
                'user_type' 	=>  'required|string|exists:user_types,id',
            ],
            [
                'email.exists'          =>  'Email not Registered',
                'email.email'           =>  'Invalid Email Format',
                'user_type.exists'      =>  'Invalid User Type'
            ]);

            if ($validator->errors()->all())                                {   return $this->kFailed($validator->errors()->first());   }

            $email 		= 	$this->validate_var(@$request->email, '');
            $password 	= 	$this->validate_var(@$request->password, '');
            $user_type 	= 	$this->validate_var(@$request->user_type, ''); 
           

            if (Auth::attempt(['email' => $email, 'password' => $password, 'user_type' => $user_type, 'status' => 1])) 
            {
                $user_id = Auth::user()->id;

                $user_data = \App\User::where('id',$user_id)->get();

                return $this->kSuccess('You are login successfully', $user_data);
            }
        	else                    {   return $this->kFailed('Wrong Credentials');  }                               
    }

    
    //*************************** Route No. 24.2 Change Password  ********************************


       
    public function changePassword(Request $request, $id)
    {
        
        $user_status = \App\User::Find($id);

        if(!$user_status) { return $this->kFailed('Unregistered User'); }
        
        
        // *********** Check for required fields ****************


            $validator = Validator::make($request->all(), [

                    'current_password'  => 'required',
                    'new_password'      => 'required',
            ]);

            if ($validator->errors()->all())                                {   return $this->kFailed($validator->errors()->first());   }


            // *********** Matching the old password and Change with new password ****************

        
            if($request->current_password != '' && $request->current_password != null)
            {
                $current_password           =       $request->current_password; 

                $password                   =       Hash::make($request->new_password);

                $current_password_data      =       \App\User::where('id', $id)->get(["id","password"]);

                if(Hash::check($current_password, $current_password_data[0]->password))
                {  
                    \App\User::where('id', $id)->update(['password' => $password]); 

                    return $this->kSuccess('Password Changed Successfully');
                } 
                else                        {   return $this->kFailed('Invalid Current Password');  }
            }
            else                            {   return $this->kFailed('Invalid Data');  }           
    }


    
    //***************************  Forgot Password Starts ********************************

    

    //================================= Route No. 24.3  Get ForgotPasswordEmail ===================================


    public function forgotPasswordEmail(Request $request)
    {
      
        // *********** Check for required fields ****************


            $validator = Validator::make($request->all(), [ 
                
                'email' => 'required|max:255|email|exists:users,email' 
            ],
            [
                'email.email'   =>  'Invalid Email Format',
                'email.exists'  =>  'Email address is not Registered'
            ]);

            if ($validator->errors()->all())                {   return $this->kFailed($validator->errors()->first());   }

            $email = $request->email;

            $email_verify = App\User::where('email', $email)->get(["first_name","id"]);
            


        // *********** Generate the Otp and send it to given email and also stored in otps table ****************     
        
                     
            $six_digit_random_number = mt_rand(100000, 999999);

            $name       =   $email_verify[0]->first_name;
            $user_id    =   $email_verify[0]->id;


            $otp_count  =   $this->check_otp_existence($user_id);
            
            if($otp_count == 1)
            {
                $otp = new \App\Otp;
                $otp->where('user_id',$user_id)->update(['otp' => $six_digit_random_number]);

            }else{

                $otp = new \App\Otp;
                $otp->create(['otp' => $six_digit_random_number,'user_id' => $user_id]);
            } 

            $user_detail = array();
            
            $user_detail = ['email' => $email, 'otp' => $six_digit_random_number,'name' => $name];
            
            
            return $this->Mailjet($user_detail);
            
            return $this->kSuccess('Your new otp sent on your email address');
    
    }



    //================================================ Route No. 24.4  OTP Verification ===================================


    public function verify_otp(Request $request)
    {
    
        // *********** Check for required fields ****************


        $validator = Validator::make($request->all(), [ 
                
            'email' =>  'required|max:255|email|exists:users,email',
            'otp'   =>  'required|exists:otps,otp' 
        ],
        [
            'email.email'   =>  'Invalid Email Format',
            'email.exists'  =>  'Email address is not Registered',
            'otp.exists'    =>  'Invalid OTP'
        ]);

        if ($validator->errors()->all())                {   return $this->kFailed($validator->errors()->first());   }
        
        
        $email = $request->email;

        $otp   = $request->otp; 

        $email_verify = App\User::where('email', $email)->get(["email","id"]);

        $user_id = $email_verify[0]->id;

        $otp_status = App\Otp::where('user_id', $user_id)->where('otp', $otp)->count();

        if($otp_status > 0)
        {
            $otp_verify = DB::table('otps')
                ->where('user_id',$user_id)
                ->where('otp',$otp)
                ->where('updated_at','<',\Carbon\Carbon::now()->subMinutes(5))
                ->delete();
                                  
            $otp_exist = App\Otp::where('user_id', $user_id)->where('otp', $otp)->get();

            if(count($otp_exist) > 0)
            {
                $extra  =   array();
                $extra['user_id']   =  $otp_exist[0]->user_id; 
                              
                return $this->kSuccess('Otp matches successfully', $extra);
            }
            else            {   return $this->kFailed('Invalid OTP');  }
            
        }
        else                {   return $this->kFailed('Invalid OTP');  }
    }


    //================================================ Route No. 24.5 Change Forgot Password  ===================================


    public function forgotPasswordChange(Request $request)
    {
    
        // *********** Check for required fields ****************

        $validator = Validator::make($request->all(), [
                
            'user_id' => 'required|exists:users,id',
            'otp'     => 'required|exists:otps,otp',
        ],
        [

            'user_id.exists'    => 'User does not exist',
            'otp.exists'        =>  'Invalid OTP'      

        ]);

        if ($validator->errors()->all())                {   return $this->kFailed($validator->errors()->first());   }


        $user_id        =  $this->validate_var(@$request->user_id,'');
        $otp            =  $this->validate_var(@$request->otp,'');
        $password       =  Hash::make($this->validate_var(@$request->password,''));
        
        $otp_status = App\Otp::where('user_id', $user_id)->where('otp', $otp)->count();

        if($otp_status < 1)         {   return $this->kFailed('Invalid OTP');   }
        
        App\User::where('id', $user_id)->update(['password' => $password]);

        return $this->kSuccess('Password Reset Successfully');
        
    }

    //***************************  Forgot Password Ends ********************************





    // //*************************** Route No. 24.6  Logout User ********************************



    // public function logout(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [

    //             'user_session_id' => 'required',
                 
    //         ]);
    //     if ($validator->errors()->all()) 
    //     {
    //             $data['status_code']    =   0;
    //             $data['status_text']    =   'Failed';             
    //             $data['message']        =   $validator->errors()->first();
    //             return $data;                   
    //     }
    //     else
    //     {
                            
    //         @\App\UserSession::where('id',@$request->user_session_id)->delete();
                            
    //             $data['status_code']    =   1;                      
    //             $data['status_text']    =   'Success';
    //             $data['message']        =   'User logged-out successfully';
    //             return $data;
    //     } 
   	//}







    // public function sendmailotp($to,$otp,$name)
    // {
    //     $curl_post_data=array(
    //       'from'    => 'noreply@clinysoft.com',
    //       'to'      => $to,
    //       'subject' => 'Reset Password OTP',
    //       'html'    => view('emails.otp',['otp' => $otp, 'name' => $name])
    //     );

    //     $service_url = "https://api.mailgun.net/v3/mailgun.ordefy.com/messages";
    //     $curl = curl_init($service_url);
    //     curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //     curl_setopt($curl, CURLOPT_USERPWD, "api:79d3805518212ed6ba57c9738006db2f-6140bac2-75cb0d5e");

    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_POST, true);

    //     curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);


    //     $curl_response = curl_exec($curl);
    //     $response = json_decode($curl_response);

    //     curl_close($curl);
    // }
}
