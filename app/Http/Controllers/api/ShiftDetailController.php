<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;
use DB;


class ShiftDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 17.1  Create Shift Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'date'                  => 'required|date_format:Y-m-d',
            'shift_type'            => 'required|exists:shift_types,id',
            'department'            => 'required|exists:departments,id',
            'description'           => 'sometimes|string|min:10',
            'shift_start_time'      => 'required|date_format:H:i',
            'shift_end_time'        => 'required|date_format:H:i',
            'break_start_time'      => 'required|date_format:H:i',
            'break_end_time'        => 'required|date_format:H:i',
            'venue'                 => 'required|string',
            'users'                 => 'required|array'
        ],
        [
            'shift_type.exists'                 => 'Invalid Shift Type',
            'department.exists'                 => 'Department does not Exist',
            'shift_start_time.date_format'      => 'Invalid Shift Start Time Format',
            'shift_end_time.date_format'        => 'Invalid Shift End Time Format',
            'break_start_time.date_format'      => 'Invalid Break Start Time Format',
            'break_end_time.date_format'        => 'Invalid Break End Time Format'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $input['users'] = implode(',',$input['users']);

        $shift_detail = \App\ShiftDetail::create($input);
        
        if($shift_detail == '')                                {   return $this->kFailed('Unable To Create Shift');  }

        return $this->kSuccess('Shift Created Successfully'); 
    }


    
    //*************************** Route No. 17.2  List all Shifts  ********************************


    public function get_list()
    {

        $per_page       =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby        =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order          =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_id        =   $this->validate_var(@$_GET['user_id'], '');
        $shift_type     =   $this->validate_var(@$_GET['shift_type'], '');
        
               
        $model      =   new App\ShiftDetail;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = DB::table('shift_details')
            ->whereRaw('FIND_IN_SET(?,users)', $user_id);  
        }

        if($shift_type != '' || $shift_type != null)
        {   
            $model = $model->where('shift_type' , $shift_type);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->shift_type = \App\ShiftType::where('id',$new_result->shift_type)->first()->title;
            $new_result->department = \App\Department::where('id',$new_result->department)->first()->title;

            $new_result->formatted_date = date('M d, Y',strtotime($new_result->date));

            $new_result->formatted_shift_start_time = date('h:i A',strtotime($new_result->shift_start_time));
            $new_result->formatted_shift_end_time = date('h:i A',strtotime($new_result->shift_end_time));
            $new_result->formatted_break_start_time = date('h:i A',strtotime($new_result->break_start_time));
            $new_result->formatted_break_end_time = date('h:i A',strtotime($new_result->break_end_time));

            $user_array = explode(',',$new_result->users);
            $new_user_array = array();

            foreach($user_array as $user)
            {
                $username = \App\User::where('id',$user)->first()->first_name;
                $new_user_array[] = $username;
            }

            $usernames = implode(',',$new_user_array);

            $new_result->users = $usernames;
                       
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Shift Detail Found');   }
                
        return $this->kSuccess('Shift List Fetched Successfully',$result);
    }


    //*************************** Route No. 17.3  Update Shift Detail ********************************


    public function update(Request $request, $id)
    {
        $shift_detail = \App\ShiftDetail::Find($id);

        if(!$shift_detail) { return $this->kFailed('Invalid Data'); }
       
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'date'                  => 'required|date_format:Y-m-d',
            'shift_type'            => 'required|exists:shift_types,id',
            'department'            => 'required|exists:departments,id',
            'description'           => 'sometimes|string|min:10',
            'shift_start_time'      => 'required|date_format:H:i',
            'shift_end_time'        => 'required|date_format:H:i',
            'break_start_time'      => 'required|date_format:H:i',
            'break_end_time'        => 'required|date_format:H:i',
            'venue'                 => 'required|string',
            'users'                 => 'required|array'
        ],
        [
            'shift_type.exists'                 => 'Invalid Shift Type',
            'department.exists'                 => 'Department does not Exist',
            'shift_start_time.date_format'      => 'Invalid Shift Start Time Format',
            'shift_end_time.date_format'        => 'Invalid Shift End Time Format',
            'break_start_time.date_format'      => 'Invalid Break Start Time Format',
            'break_end_time.date_format'        => 'Invalid Break End Time Format'
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();



        $shift_detail = $shift_detail->update($input);

        return $this->kSuccess('Shift Updated Successfully');
    }


    //*************************** Route No. 17.4  Delete Shift Detail ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $shift_detail = \App\ShiftDetail::Find($id);

        if(!$shift_detail) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from staff_leaves table ****************

        $shift_detail->where('id',$id)->delete();

        return $this->kSuccess('Shift Deleted Successfully');
    }

}
