<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class SocialMediaTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 4.1  Create Social Media Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:social_media_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Social Media Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $social_media_type = \App\SocialMediaType::create($input);

        if($social_media_type == '')                                {   return $this->kFailed('Unable To Create Social Media Type');  }

        return $this->kSuccess('Social Media Type Created Successfully'); 
    }


    
    //*************************** Route No. 4.2  Get Social Media Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\SocialMediaType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Social Media Type Found');   }
                
        return $this->kSuccess('Social Media Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 4.3  Update Social Media Type  ********************************


    public function update(Request $request, $id)
    {
         
        $social_media_type = \App\SocialMediaType::Find($id);

        if(!$social_media_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:social_media_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Social Media Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $social_media_type = $social_media_type->update($input);

        return $this->kSuccess('Social Media Type Updated Successfully');
    }


    //*************************** Route No. 4.4  Delete Social Media Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $social_media_type = \App\SocialMediaType::Find($id);

        if(!$social_media_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Social Media Type ****************

        // $social_media_type_usage = \App\UserSocialMedia::where('social_media_type',$id)->count();

        // if($social_media_type_usage > 0)    {  return $this->kFailed('Social Media Type Assigned to User'); }

        
        // *********** Delete data from social_media_types table ****************

        $social_media_type->where('id',$id)->delete();

        return $this->kSuccess('Social Media Type Deleted Successfully');
    }


    //*************************** Route No. 4.5  Update Social Media Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $social_media_type = \App\SocialMediaType::Find($id);

        if(!$social_media_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $social_media_type_status = $social_media_type->update([ 'status' => $request->status ]);

        $result = \App\SocialMediaType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
