<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class VehicleTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 8.1  Create Vehicle Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:vehicle_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Vehicle Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $vehicle_type = \App\VehicleType::create($input);

        if($vehicle_type == '')                                {   return $this->kFailed('Unable To Create Vehicle Type');  }

        return $this->kSuccess('Vehicle Type Created Successfully'); 
    }


    
    //*************************** Route No. 8.2  Get Vehicle Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\VehicleType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Vehicle Type Found');   }
                
        return $this->kSuccess('Vehicle Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 8.3  Update Vehicle Type  ********************************


    public function update(Request $request, $id)
    {
         
        $vehicle_type = \App\VehicleType::Find($id);

        if(!$vehicle_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:vehicle_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Vehicle Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $vehicle_type = $vehicle_type->update($input);

        return $this->kSuccess('Vehicle Type Updated Successfully');
    }


    //*************************** Route No. 8.4  Delete Vehicle Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $vehicle_type = \App\VehicleType::Find($id);

        if(!$vehicle_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Vehicle Type ****************

        // $vehicle_type_usage = \App\UserVehicleDetail::where('vehicle_type',$id)->count();

        // if($vehicle_type_usage > 0)    {  return $this->kFailed('Vehicle Type Assigned to User'); }

        
        // *********** Delete data from vehicle_types table ****************

        $vehicle_type->where('id',$id)->delete();

        return $this->kSuccess('Vehicle Type Deleted Successfully');
    }


    //*************************** Route No. 8.5  Update Vehicle Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $vehicle_type = \App\VehicleType::Find($id);

        if(!$vehicle_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $vehicle_type_status = $vehicle_type->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\VehicleType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
