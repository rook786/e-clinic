<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class StaffLeaveController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 15.1  Create Staff Leave  ********************************
    
    
    public function store(Request $request)
    {
        
        if(!$request->leave_type) { return $this->kFailed('Leave Type Field is Required');  }
        
        $title = \App\LeaveType::Find($request->leave_type);
        if(!$title) { return $this->kFailed('Invalid Leave Type'); }

        $request['title'] = $title['title'];
        
               
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'from_date'             => 'required|date_format:Y-m-d',
            'from_time'             => 'required_if:title,Short Leave,Half Day Leave|date_format:H:i',
            'to_time'               => 'required_if:title,Short Leave,Half Day Leave|date_format:H:i',
            'no_of_days'            => 'required_if:title,Casual Leave,Earned Leave,Medical Leave|not_in:0',
            'to_date'               => 'required_unless:no_of_days,1,""|date_format:Y-m-d',
            'reason'                => 'required|string',
            'replacement'           => 'sometimes|exists:users,id'
        ],
        [
            'user_id.exists'            => 'Staff Does not Exist',
            'to_date.required_unless'   => 'The to date field is required',
            'no_of_days.required_if'    => 'The no of days field is required',
            'no_of_days.not_in'         => 'Invalid no of days',
            'from_time.required_if'     => 'The from time field is required',
            'to_time.required_if'       => 'The to time field is required',
            'from_date.date_format'     => 'Invalid From Date Format',
            'to_date.date_format'       => 'Invalid To Date Format',
            'from_time.date_format'     => 'Invalid From Time Format',
            'to_time.date_format'       => 'Invalid To Time Format'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $staff_leave = \App\StaffLeave::create($input);

        if($staff_leave == '')                                {   return $this->kFailed('Unable To Create Staff Leave');  }

        return $this->kSuccess('Staff Leave Created Successfully'); 
    }


    
    //*************************** Route No. 15.2  List all Leaves  ********************************



    public function get_list()
    {

        $per_page       =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby        =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order          =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_id        =   $this->validate_var(@$_GET['user_id'], '');
        $leave_type     =   $this->validate_var(@$_GET['leave_type'], '');
        
               
        $model      =   new App\StaffLeave;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }

        if($leave_type != '' || $leave_type != null)
        {   
            $model = $model->where('leave_type' , $leave_type);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->leave_type = \App\LeaveType::where('id',$new_result->leave_type)->first()->title;
            $new_result->user_name = \App\User::where('id',$new_result->user_id)->first()->first_name;

            if($new_result->from_date != '')
            {
                $new_result->formatted_from_date = date('M d, Y',strtotime($new_result->from_date));
            }

            if($new_result->to_date != '')
            {
                $new_result->formatted_to_date = date('M d, Y',strtotime($new_result->to_date));
            }

            if($new_result->from_time != '')
            {
                $new_result->formatted_from_time = date('h:i A',strtotime($new_result->from_time));
            }

            if($new_result->to_time != '')
            {
                $new_result->formatted_to_time = date('h:i A',strtotime($new_result->to_time));
            }
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Leave Found');   }
                
        return $this->kSuccess('Leave List Fetched Successfully',$result);
    }


    //*************************** Route No. 15.3  Update Staff Leave  ********************************


    public function update(Request $request, $id)
    {
         
        $staff_leave = \App\StaffLeave::Find($id);

        if(!$staff_leave) { return $this->kFailed('Invalid Data'); }

        if(!$request->leave_type) { return $this->kFailed('Leave Type Field is Required');  }
        
        $title = \App\LeaveType::Find($request->leave_type);
        if(!$title) { return $this->kFailed('Invalid Leave Type'); }

        $request['title'] = $title['title'];

        
        // *********** Check for required fields ****************

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'               => 'required|exists:users,id',
            'from_date'             => 'required|date_format:Y-m-d',
            'from_time'             => 'required_if:title,Short Leave,Half Day Leave|date_format:H:i',
            'to_time'               => 'required_if:title,Short Leave,Half Day Leave|date_format:H:i',
            'no_of_days'            => 'required_if:title,Casual Leave,Earned Leave,Medical Leave|not_in:0',
            'to_date'               => 'required_unless:no_of_days,1,""|date_format:Y-m-d',
            'reason'                => 'required|string',
            'replacement'           => 'sometimes|exists:users,id'
        ],
        [
            'user_id.exists'            => 'Staff Does not Exist',
            'to_date.required_unless'   => 'The to date field is required',
            'no_of_days.required_if'    => 'The no of days field is required',
            'no_of_days.not_in'         => 'Invalid no of days',
            'from_time.required_if'     => 'The from time field is required',
            'to_time.required_if'       => 'The to time field is required',
            'from_date.date_format'     => 'Invalid From Date Format',
            'to_date.date_format'       => 'Invalid To Date Format',
            'from_time.date_format'     => 'Invalid From Time Format',
            'to_time.date_format'       => 'Invalid To Time Format'
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $staff_leave = $staff_leave->update($input);

        return $this->kSuccess('Staff Leave Updated Successfully');
    }


    //*************************** Route No. 15.4  Delete Staff Leave  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $staff_leave = \App\StaffLeave::Find($id);

        if(!$staff_leave) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from staff_leaves table ****************

        $staff_leave->where('id',$id)->delete();

        return $this->kSuccess('Staff Leave Deleted Successfully');
    }

}
