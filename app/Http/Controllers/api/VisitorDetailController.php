<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class VisitorDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 13.1  Create Visitor Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'date'                  => 'required|date_format:Y-m-d',
            'time'                  => 'required|date_format:H:i',
            'visitor_type'          => 'required|exists:visitor_types,id',
            'first_name'            => 'required|string|min:2',
            'mobile'                => 'required|max:12',
            'visiting_reason'       => 'required',
            'address'               => 'required',
            'designation'           => 'required|exists:user_types,id',
            'concerned_person'      => 'required|exists:users,id',
            'department'            => 'required|exists:departments,id'
        ],
        [
            'date.date_format'      => 'Invalid Date Format',
            'time.date_format'      => 'Invalid Time Format',
            'visitor_type.exists'   => 'Invalid Visitor Type',
            'designation.exists'    => 'Invalid Designation',
            'concerned_person.exists'   => 'Person Does not Exist',
            'department.exists'     => 'Invalid Department',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $visitor_detail = \App\VisitorDetail::create($input);

        if($visitor_detail == '')                                {   return $this->kFailed('Unable To Create Visitor Detail');  }

        return $this->kSuccess('Visitor Detail Created Successfully'); 
    }


    
    //*************************** Route No. 13.2  Get Visitor List  ********************************



    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $date               =   $this->validate_var(@$_GET['date'], '');
        $month              =   $this->validate_var(@$_GET['month'], '');
        $year               =   $this->validate_var(@$_GET['year'], '');
        $visitor_type       =   $this->validate_var(@$_GET['visitor_type'], '');
               
        $model      =   new App\VisitorDetail;
              
        if($date != '' || $date != null)
        {   
            $model = $model->where('date' , $date);  
        }

        if($month != '' || $month != null)
        {   
            $model = $model->whereMonth('date' , $month);  
        }

        if($year != '' || $year != null)
        {   
            $model = $model->whereYear('date' , $year);  
        }

        if($visitor_type != '' || $visitor_type != null)
        {   
            $model = $model->where('visitor_type' , $visitor_type);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->formatted_date = date('M d, Y',strtotime($new_result->date));
            $new_result->formatted_time = date('h:i A',strtotime($new_result->time));
            $new_result->visitor_type = \App\VisitorType::where('id',$new_result->visitor_type)->first()->title;
            $new_result->designation = \App\UserType::where('id',$new_result->designation)->first()->title;
            $new_result->concerned_person = \App\User::where('id',$new_result->concerned_person)->first()->first_name;
            $new_result->department = \App\Department::where('id',$new_result->department)->first()->title;
            
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Visitor Detail Found');   }
                
        return $this->kSuccess('Visitor List Fetched Successfully',$result);
    }


    //*************************** Route No. 13.3  Update Visitor Detail  ********************************


    public function update(Request $request, $id)
    {
         
        $visitor_detail = \App\VisitorDetail::Find($id);

        if(!$visitor_detail) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'date'                  => 'required|date_format:Y-m-d',
            'time'                  => 'required|date_format:H:i',
            'visitor_type'          => 'required|exists:visitor_types,id',
            'first_name'            => 'required|string|min:2',
            'mobile'                => 'required|max:12',
            'visiting_reason'       => 'required',
            'address'               => 'required',
            'designation'           => 'required|exists:user_types,id',
            'concerned_person'      => 'required|exists:users,id',
            'department'            => 'required|exists:departments,id'
        ],
        [
            'date.date_format'      => 'Invalid Date Format',
            'time.date_format'      => 'Invalid Time Format',
            'visitor_type.exists'   => 'Invalid Visitor Type',
            'designation.exists'    => 'Invalid Designation',
            'concerned_person.exists'   => 'Person Does not Exist',
            'department.exists'     => 'Invalid Department',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $visitor_detail = $visitor_detail->update($input);

        return $this->kSuccess('Visitor Detail Updated Successfully');
    }


    //*************************** Route No. 13.4  Delete Visitor Detail  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $visitor_detail = \App\VisitorDetail::Find($id);

        if(!$visitor_detail) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from visitor_types table ****************

        $visitor_detail->where('id',$id)->delete();

        return $this->kSuccess('Visitor Detail Deleted Successfully');
    }

}
