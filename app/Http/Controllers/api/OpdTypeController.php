<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class OpdTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 5.1  Create Opd Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:opd_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Opd Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $opd_type = \App\OpdType::create($input);

        if($opd_type == '')                                {   return $this->kFailed('Unable To Create Opd Type');  }

        return $this->kSuccess('Opd Type Created Successfully'); 
    }


    
    //*************************** Route No. 5.2  Get Opd Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\OpdType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Opd Type Found');   }
                
        return $this->kSuccess('Opd Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 5.3  Update Opd Type  ********************************


    public function update(Request $request, $id)
    {
         
        $opd_type = \App\OpdType::Find($id);

        if(!$opd_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:opd_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Opd Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $opd_type = $opd_type->update($input);

        return $this->kSuccess('Opd Type Updated Successfully');
    }


    //*************************** Route No. 5.4  Delete Opd Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $opd_type = \App\OpdType::Find($id);

        if(!$opd_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Opd Type ****************

        // $opd_type_usage = \App\OpdDetail::where('opd_type',$id)->count();

        // if($opd_type_usage > 0)    {  return $this->kFailed('Opd Type Assigned to Opd'); }

        
        // *********** Delete data from opd_types table ****************

        $opd_type->where('id',$id)->delete();

        return $this->kSuccess('Opd Type Deleted Successfully');
    }


    //*************************** Route No. 5.5  Update Opd Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $opd_type = \App\OpdType::Find($id);

        if(!$opd_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $opd_type_status = $opd_type->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\OpdType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
