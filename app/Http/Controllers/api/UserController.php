<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;
use Hash;
use File;


class UserController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 23.1  Create User  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'department'            => 'required|exists:departments,id',
            'user_type'             => 'required|exists:user_types,id',
            'designation'           => 'required|exists:designations,id',
            'first_name'            => 'required|string|min:2',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|string',
            'mobile'                => 'required|unique:users,mobile|max:12',
        ],
        [
            'department.exists'     => 'Invalid Department',
            'user_type.exists'      => 'Invalid User Type',
            'designation.exists'    => 'Invalid Designation',
            'email.unique'          => 'Email Already Exist',
            'mobile.unique'         => 'Mobile Number Already Exist',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        $user = \App\User::create($input);

        $user_id = $user['id'];


        // *********** Store data into user_details table ****************

        $user_details = new App\UserDetail;
        $user_details->user_id = $user_id;
        $user_details->save();


        // *********** Store data into user_bank_details table ****************

        $user_details = new App\UserBankDetail;
        $user_details->user_id = $user_id;
        $user_details->save();


        // *********** Store data into user_salary_details table ****************

        $user_details = new App\UserSalaryDetail;
        $user_details->user_id = $user_id;
        $user_details->save();

        
        // *********** Update data into users table ****************

               
        $prefix = \App\Setting::where('key','emp_id')->first()->value;
        
        $emp_id = $prefix.$user_id;

        @App\User::where('id',$user_id)->update([

            'emp_id' => $emp_id

        ]);

        if($user == '')                                {   return $this->kFailed('Unable To Create User');  }

        return $this->kSuccess('User Created Successfully'); 
    }


    
    //*************************** Route No. 23.2  Get User List  ********************************



    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_type          =   $this->validate_var(@$_GET['user_type'], '');
        $department         =   $this->validate_var(@$_GET['department'], '');
        $designation         =   $this->validate_var(@$_GET['designation'], '');
        $user_id            =   $this->validate_var(@$_GET['user_id'], '');
               
        $model      =   new App\User;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('id' , $user_id);  
        }

        if($user_type != '' || $user_type != null)
        {   
            $model = $model->where('user_type' , $user_type);  
        }

        if($department != '' || $department != null)
        {   
            $model = $model->where('department' , $department);  
        }

        if($designation != '' || $designation != null)
        {   
            $model = $model->where('designation' , $department);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->user_type = \App\UserType::where('id',$new_result->user_type)->first()->title;
            $new_result->department = \App\Department::where('id',$new_result->department)->first()->title;
            $new_result->designation = \App\Designation::where('id',$new_result->designation)->first()->title;
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No User Found');   }
                
        return $this->kSuccess('User List Fetched Successfully',$result);
    }


    //*************************** Route No. 23.3  Update User  ********************************


    public function update(Request $request, $id)
    {
         
        $user = \App\User::Find($id);

        if(!$user) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'department'            => 'required|exists:departments,id',
            'user_type'             => 'required|exists:user_types,id',
            'designation'           => 'required|exists:designations,id',
            'first_name'            => 'required|string|min:2',
            'email'                 => 'required|email|unique:users,email,'.$id,
            'mobile'                => 'required|unique:users,mobile,'.$id.'|max:12'
        ],
        [
            'department.exists'     => 'Invalid Department',
            'user_type.exists'      => 'Invalid User Type',
            'designation.exists'    => 'Invalid Designation',
            'email.unique'          => 'Email Already Exist',
            'mobile.unique'         => 'Mobile Number Already Exist',
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $user = $user->update($input);

        return $this->kSuccess('User Updated Successfully');
    }


    //*************************** Route No. 23.4  Delete User  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $user = \App\User::Find($id);

        if(!$user) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from users table ****************

        $user->where('id',$id)->delete();

        return $this->kSuccess('User Deleted Successfully');
    }



    //*************************** Route No. 23.5  Update User Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $user = \App\User::Find($id);

        if(!$user) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $user_status = $user->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\User::where('id',$id)->get();


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }



    //*************************** Route No. 23.6 Get User Profile  ********************************


    public function get_user_detail($id)
    {
        
        $main_array = array();
        
        $personal_info_array = array();
        $additional_info_array = array();
        $bank_info_array = array();
        $salary_info_array = array();
        //$document_info_array = array();
    

            
        $personal_info_array['title'] = 'Personal';
        $personal_info_array['identifier'] = 'personal';
        $personal_info_array['type']  = 'fields';

    

        //--------- fields array main --------------
        
        $fields_array = array();
       
        
        //-----------fields array1
        
        $fields_array1=array();

        $fields_array1['title'] = 'Department';
        $fields_array1['identifier'] = 'department';

        $department =  @\App\User::where('id',$id)->first()->department;
        if($department!='')
        {
        $fields_array1['value']  = $department;
        }
        else
        {
        $fields_array1['value'] ='';
        }

        $fields_array[] = $fields_array1;


        //-----------fields array2
        
        $fields_array2=array();

        $fields_array2['title'] = 'User Type';
        $fields_array2['identifier'] = 'user_type';

        $user_type =  @\App\User::where('id',$id)->first()->user_type;
        if($user_type!='')
        {
        $fields_array2['value']  = $user_type;
        }
        else
        {
        $fields_array2['value'] ='';
        }

        $fields_array[] = $fields_array2;



        //-----------fields array3
        
        $fields_array3=array();

        $fields_array3['title'] = 'Designation';
        $fields_array3['identifier'] = 'designation';

        $designation =  @\App\User::where('id',$id)->first()->designation;
        
        if($designation!='')
        {
        $fields_array3['value']  = $designation; 
        }
        else
        {
        $fields_array3['value'] ='';
        }

        $fields_array[] = $fields_array3;



        //-----------fields array4
        
        $fields_array4=array();

        $fields_array4['title'] = 'First Name';
        $fields_array4['identifier'] = 'first_name';

        $first_name =  @\App\User::where('id',$id)->first()->first_name;
        
        if($first_name!='')
        {
        $fields_array4['value']  = $first_name; 
        }
        else
        {
        $fields_array4['value'] ='';
        }

        $fields_array[] = $fields_array4;



        //-----------fields array5
        
        $fields_array5=array();

        $fields_array5['title'] = 'Last Name';
        $fields_array5['identifier'] = 'last_name';

        $last_name =  @\App\User::where('id',$id)->first()->last_name;
        
        if($last_name!='')
        {
        $fields_array5['value']  = $last_name; 
        }
        else
        {
        $fields_array5['value'] ='';
        }

        $fields_array[] = $fields_array5;
        
               
        
        
        //-----------fields array6
        
        $fields_array6=array();

        $fields_array6['title'] = 'Email';
        $fields_array6['identifier'] = 'email';

        $email =  @\App\User::where('id',$id)->first()->email;
        if($email!='')
        {
        $fields_array4['value']  = $email;
        }
        else
        {
        $fields_array4['value'] ='';
        }

        $fields_array[] = $fields_array6;



        //-----------fields array7
        
        $fields_array7=array();

        $fields_array7['title'] = 'Mobile No';
        $fields_array7['identifier'] = 'mobile';

        $mobile_no =  @\App\User::where('id',$id)->first()->mobile;
        if($mobile_no!='')
        {
        $fields_array5['value']  = $mobile_no;
        }
        else
        {
        $fields_array5['value'] ='';
        }

        $fields_array[] = $fields_array7;


        $personal_info_array['fields'] = $fields_array;

        $main_array[] = $personal_info_array;
        

        //******************** Personal Information block ends here *****************


        $additional_info_array['title'] = 'Additional';
        $additional_info_array['identifier'] = 'additional';
        $additional_info_array['type']  = 'fields';

        
        //--------- fields array main====
        
        $fields_array = array();
       

        //-----------fields array1
        
        $fields_array4=array();

        $fields_array1['title'] = 'Date of Birth';
        $fields_array1['identifier'] = 'date_of_birth';

        $date_of_birth =  @\App\UserDetail::where('user_id',$id)->first()->date_of_birth;

        if($date_of_birth != '')
        {
            $fields_array1['value']  = $date_of_birth;
        }
        else
        {
        $fields_array1['value'] ='';
        }

        $fields_array[] = $fields_array1;


        //-----------fields array2
        
        $fields_array2=array();

        $fields_array2['title'] = 'Date of Joining';
        $fields_array2['identifier'] = 'date_of_joining';

        $date_of_joining =  @\App\UserDetail::where('user_id',$id)->first()->date_of_joining;

        if($date_of_joining != '')
        {
            $fields_array2['value']  = $date_of_joining;
        }
        else
        {
        $fields_array2['value'] ='';
        }

        $fields_array[] = $fields_array2;



        //-----------fields array3
        
        
        $fields_array3=array();

        $fields_array3['title'] = 'Current Address';
        $fields_array3['identifier'] = 'current_address';

        $current_address =  @\App\UserDetail::where('user_id',$id)->first(['current_address'])->current_address;
        if($current_address!='')
        {
        $fields_array3['value']  = $current_address;
        }
        else
        {
        $fields_array3['value'] ='';
        }

        $fields_array[] = $fields_array3;

        
        
        //-----------fields array4
        
        
        $fields_array4=array();

        $fields_array4['title'] = 'Permanent Address';
        $fields_array4['identifier'] = 'permanent_address';

        $permanent_address =  @\App\UserDetail::where('user_id',$id)->first()->permanent_address;
        if($permanent_address!='')
        {
        $fields_array4['value']  = $permanent_address;
        }
        else
        {
        $fields_array4['value'] ='';
        }

        $fields_array[] = $fields_array4;


        //-----------fields array5
        
        $fields_array5=array();

        $fields_array5['title'] = 'Whatsapp No';
        $fields_array5['identifier'] = 'whatsapp_no';

        $whatsapp_no =  @\App\UserDetail::where('user_id',$id)->first()->whatsapp_no;

        if($whatsapp_no != '')
        {
            $fields_array5['value']  = $whatsapp_no;
        }
        else
        {
        $fields_array5['value'] ='';
        }

        $fields_array[] = $fields_array5;


        //-----------fields array6
        
        $fields_array6=array();

        $fields_array6['title'] = 'Alternate Email';
        $fields_array6['identifier'] = 'alternate_email';

        $alternate_email =  @\App\UserDetail::where('user_id',$id)->first()->alternate_email;

        if($alternate_email != '')
        {
            $fields_array6['value']  = $alternate_email;
        }
        else
        {
        $fields_array6['value'] ='';
        }

        $fields_array[] = $fields_array6;



        //-----------fields array7
        
        $fields_array7=array();

        $fields_array7['title'] = 'Alternate Mobile';
        $fields_array7['identifier'] = 'alternate_mobile';

        $alternate_mobile =  @\App\UserDetail::where('user_id',$id)->first()->alternate_mobile;

        if($alternate_mobile != '')
        {
            $fields_array7['value']  = $alternate_mobile;
        }
        else
        {
        $fields_array7['value'] ='';
        }

        $fields_array[] = $fields_array7;



        //-----------fields array8
        
        $fields_array8=array();

        $fields_array8['title'] = 'Practice Licence No';
        $fields_array8['identifier'] = 'practice_licence_no';

        $practice_licence_no =  @\App\UserDetail::where('user_id',$id)->first()->practice_licence_no;

        if($alternate_mobile != '')
        {
            $fields_array8['value']  = $practice_licence_no;
        }
        else
        {
        $fields_array8['value'] ='';
        }

        $fields_array[] = $fields_array8;


        $additional_info_array['fields'] = $fields_array;

        $main_array[] = $additional_info_array;


        //******************** Additional Information block ends here *****************


        $bank_info_array['title'] = 'Bank Details';
        $bank_info_array['identifier'] = 'bank_details';
        $bank_info_array['type']  = 'fields';

    
        //--------- fields array main====
        
        $fields_array = array();

        
        //-----------fields array1
        
        $fields_array1=array();

        $fields_array1['title'] = 'Bank';
        $fields_array1['identifier'] = 'bank_name';

        $bank_name =  @\App\UserBankDetail::where('user_id',$id)->first(['bank_name'])->bank_name;

        if($bank_name!='')
        {
        $fields_array1['value']  =  $bank_name; 
        }
        else
        {
        $fields_array1['value'] ='';
        }

        $fields_array[] = $fields_array1;


        //-----------fields array2


        $fields_array2['title'] = 'Account Holder Name';
        $fields_array2['identifier'] = 'account_name';

        $account_name =  @\App\UserBankDetail::where('user_id',$id)->first()->account_name;

        if($account_name!='')
        {
        $fields_array2['value']  =  $account_name; 
        }
        else
        {
        $fields_array2['value'] ='';
        }

        $fields_array[] = $fields_array2;


        //-----------fields array3
        
        $fields_array2=array();

        $fields_array3['title'] = 'Account No';
        $fields_array3['identifier'] = 'account_no';

        $account_no =  @\App\UserBankDetail::where('user_id',$id)->first()->account_no;

        if($account_no!='')
        {
        $fields_array3['value']  =  $account_no; 
        }
        else
        {
        $fields_array3['value'] ='';
        }

        $fields_array[] = $fields_array3;



        //-----------fields array4
        
        $fields_array4=array();

        $fields_array4['title'] = 'IFSC Code';
        $fields_array4['identifier'] = 'ifsc_code';

        $ifsc_code =  @\App\UserBankDetail::where('user_id',$id)->first()->ifsc_code;

        if($ifsc_code!='')
        {
        $fields_array4['value']  =  $ifsc_code; 
        }
        else
        {
        $fields_array4['value'] ='';
        }

        $fields_array[] = $fields_array4;



        //-----------fields array5
        
        $fields_array5=array();

        $fields_array5['title'] = 'Branch';
        $fields_array5['identifier'] = 'branch';

        $branch =  @\App\UserBankDetail::where('user_id',$id)->first()->branch;

        if($branch!='')
        {
        $fields_array5['value']  =  $branch; 
        }
        else
        {
        $fields_array5['value'] ='';
        }

        $fields_array[] = $fields_array5;


        $bank_info_array['fields'] = $fields_array;

        $main_array[] = $bank_info_array;


        //******************** Bank Information block ends here *****************


        $salary_info_array['title'] = 'Salary Details';
        $salary_info_array['identifier'] = 'salary_details';
        $salary_info_array['type']  = 'fields';


        //-----------fields array1
        
        $fields_array1=array();

        $fields_array1['title'] = 'Basic Salary';
        $fields_array1['identifier'] = 'basic_salary';

        $basic_salary =  @\App\UserSalaryDetail::where('user_id',$id)->first()->basic_salary;

        if($basic_salary!='')
        {
        $fields_array1['value']  =  $basic_salary; 
        }
        else
        {
        $fields_array1['value'] ='';
        }

        $fields_array[] = $fields_array1;


        //-----------fields array2
        
        $fields_array2=array();

        $fields_array2['title'] = 'Dearness Allowance';
        $fields_array2['identifier'] = 'dearness_allowance';

        $dearness_allowance =  @\App\UserSalaryDetail::where('user_id',$id)->first()->dearness_allowance;

        if($dearness_allowance!='')
        {
        $fields_array2['value']  =  $dearness_allowance; 
        }
        else
        {
        $fields_array2['value'] ='';
        }

        $fields_array[] = $fields_array2;


        //-----------fields array3
        
        $fields_array3=array();

        $fields_array3['title'] = 'House Rent Allowance';
        $fields_array3['identifier'] = 'house_rent_allowance';

        $house_rent_allowance =  @\App\UserSalaryDetail::where('user_id',$id)->first()->house_rent_allowance;

        if($house_rent_allowance!='')
        {
        $fields_array3['value']  =  $house_rent_allowance; 
        }
        else
        {
        $fields_array3['value'] ='';
        }

        $fields_array[] = $fields_array3;


        //-----------fields array4
        
        $fields_array4=array();

        $fields_array4['title'] = 'Leave Travel Allowance';
        $fields_array4['identifier'] = 'leave_travel_allowance';

        $leave_travel_allowance =  @\App\UserSalaryDetail::where('user_id',$id)->first()->leave_travel_allowance;

        if($leave_travel_allowance!='')
        {
        $fields_array4['value']  =  $leave_travel_allowance; 
        }
        else
        {
        $fields_array4['value'] ='';
        }

        $fields_array[] = $fields_array4;



        //-----------fields array5
        
        $fields_array5=array();

        $fields_array5['title'] = 'Conveyance Allowance';
        $fields_array5['identifier'] = 'conveyance_allowance';

        $conveyance_allowance =  @\App\UserSalaryDetail::where('user_id',$id)->first()->conveyance_allowance;

        if($conveyance_allowance!='')
        {
        $fields_array5['value']  =  $conveyance_allowance; 
        }
        else
        {
        $fields_array5['value'] ='';
        }

        $fields_array[] = $fields_array5;



        //-----------fields array6
        
        $fields_array6=array();

        $fields_array6['title'] = 'Medical Allowance';
        $fields_array6['identifier'] = 'medical_allowance';

        $medical_allowance =  @\App\UserSalaryDetail::where('user_id',$id)->first()->medical_allowance;

        if($medical_allowance!='')
        {
        $fields_array6['value']  =  $medical_allowance; 
        }
        else
        {
        $fields_array6['value'] ='';
        }

        $fields_array[] = $fields_array6;



        //-----------fields array7
        
        $fields_array7=array();

        $fields_array7['title'] = 'Education Allowance';
        $fields_array7['identifier'] = 'education_allowance';

        $education_allowance =  @\App\UserSalaryDetail::where('user_id',$id)->first()->education_allowance;

        if($education_allowance!='')
        {
        $fields_array7['value']  =  $education_allowance; 
        }
        else
        {
        $fields_array7['value'] ='';
        }

        $fields_array[] = $fields_array7;



        //-----------fields array8
        
        $fields_array8=array();

        $fields_array8['title'] = 'Hostel Allowance';
        $fields_array8['identifier'] = 'hostel_allowance';

        $hostel_allowance =  @\App\UserSalaryDetail::where('user_id',$id)->first()->hostel_allowance;

        if($hostel_allowance!='')
        {
        $fields_array8['value']  =  $hostel_allowance; 
        }
        else
        {
        $fields_array8['value'] ='';
        }

        $fields_array[] = $fields_array8;


        $salary_info_array['fields'] = $fields_array;

        $main_array[] = $salary_info_array;


        //******************** Salary Information block ends here *****************


        if(sizeof($main_array) == 0)     {   return $this->kFailed('Result Not Found');  }
                                          
        return $this->kSuccess('User Detail Fetched Successfully', $main_array);
   
    }



    //*************************** Route No. 23.7 Update User Profile  ********************************


    public function update_user_profile(Request $request, $id)
    {


        $input = $request->all();
        $type = $input['type'];

        if($type=='basic')
        {
            $basic_array = $input['fields'];
            $update_array = array();
         
                $update_array[$basic_array['identifier']] = $basic_array['value'];
            
            $result = @\App\User::where('id',$id)->update($update_array);
        }

        if($type=='additional')
        {
            $additional_array = $input['fields'];
            $update_array = array();
           

                $update_array[$additional_array['identifier']] = $additional_array['value'];
           
            $result = @\App\UserDetail::where('user_id',$id)->update($update_array);
        }

        if($type=='bank')
        {
            $bank_array = $input['fields'];
            $update_array = array();
            

                $update_array[$bank_array['identifier']] = $bank_array['value'];
           
            $result = @\App\UserBankDetail::where('user_id',$id)->update($update_array);
        }


        if($type=='salary')
        {
            $salary_array = $input['fields'];
            $update_array = array();
            

                $update_array[$salary_array['identifier']] = $salary_array['value'];
           
            $result = @\App\UserSalaryDetail::where('user_id',$id)->update($update_array);
        }


        return $this->kSuccess('User Detail Updated Successfully');
    }



    //*************************** Route No. 23.8  Upload Profile Image  ********************************


    public function upload_profile_image(Request $request)
    {

    	$validator  =   Validator::make($request->all(), [

            'user_id'           =>   'required|exists:users,id',
            'profile_image'     =>   'required|mimes:jpeg,jpg,png'
        ],
        [
            'user_id.exists'        =>   'User does not exist',
            'profile_image.mimes'   =>   'Unsupported Image Format'
        ]);

        if ($validator->errors()->all())                                {   return $this->kFailed($validator->errors()->first());   }
            
        //for file upload

        $path = public_path().'/images/users/'.$request->user_id;

        if(!File::exists($path))        {   File::makeDirectory($path, $mode = 0777, true, true);   }
        
        if(isset($request->profile_image) && !empty($request->profile_image))
        {
            $unique_string      =   'profile_image-'.strtotime(date('Y-m-d h:i:s'));    
            $file               =   $request->profile_image;                       
            $photo_name         =   $unique_string.$file->getClientOriginalName();
        
            $thumb_name_main    =   $photo_name; 
            $thumb_name         =   "thumb_".$photo_name; 
            $file->move($path,$photo_name);
            $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'200'); 
        }
        else
        {
            $thumb_name_main = '';
            $thumb_name = '';   
        }

        $image_url = $request->user_id.'/'.$thumb_name_main;
        $thumb_image_url = $request->user_id.'/'.$thumb_name;
    
        \App\User::where('id',$request->user_id)->update([

            'profile_image'	=>	$image_url,
            'profile_image_thumb' => $thumb_image_url	
        ]);

        return $this->kSuccess('Profile Image Uploaded Successfully');
    }







    public function make_thumb($src, $dest, $desired_width)
    {

        /* read the source image */

        $width                                               =  @imagesx($source_image);
        $height                                              =  @imagesy($source_image);
            
        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefromjpeg($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($height == '' || $height == null)
        {
           $source_image                                    =   @imagecreatefrompng($src);
           $width                                           =   @imagesx($source_image);
           $height                                          =   @imagesy($source_image);
        }

        if($width < 1)                                      {   return 1;   }


        /* find the "desired height" of this thumbnail, relative to the desired width  */

        $desired_height                                     =   floor($height * ($desired_width / $width));

        /* create a new, "virtual" image */

        $virtual_image                                      =   imagecreatetruecolor($desired_width, $desired_height);

        /* copy source image at a resized size */

        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

        /* create the physical thumbnail image to its destination */

        imagejpeg($virtual_image, $dest);

        
    }

}
