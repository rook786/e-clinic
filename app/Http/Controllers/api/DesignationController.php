<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class DesignationController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 1.1  Create Designation  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'department'    =>  'required|exists:departments,id',
            'title'         => 'required|string|unique:user_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Designation Already Exist',
            'department.exists' => 'Invalid Department'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $designation = \App\Designation::create($input);

        if($designation == '')                                {   return $this->kFailed('Unable To Create Designation');  }

        return $this->kSuccess('Designation Created Successfully'); 
    }


    
    //*************************** Route No. 1.2  Get Designation List  ********************************



    public function get_list()
    {

        $per_page       =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby        =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order          =   $this->validate_var(@$_GET['order'], 'DESC');
        $status         =   $this->validate_var(@$_GET['status'], '');
        $department     =   $this->validate_var(@$_GET['department'], '');
               
        $model      =   new App\Designation;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        if($department != '' || $department != null)
        {   
            $model = $model->where('department' , $department);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','department','title','identifier','status'])->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->department = \App\Department::where('id',$new_result->department)->first()->title;
        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Designation Found');   }
                
        return $this->kSuccess('Designation List Fetched Successfully',$result);
    }


    //*************************** Route No. 1.3  Update Designation  ********************************


    public function update(Request $request, $id)
    {
         
        $designation = \App\Designation::Find($id);

        if(!$designation) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'department'    =>  'required|exists:departments,id',
            'title'         => 'required|string|unique:user_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Designation Already Exist',
            'department.exists' => 'Invalid Department'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $designation = $designation->update($input);

        return $this->kSuccess('Designation Updated Successfully');
    }


    //*************************** Route No. 1.4  Delete Designation  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $designation = \App\Designation::Find($id);

        if(!$designation) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Designation ****************

        // $designation_usage = \App\User::where('user_type',$id)->count();

        // if($designation_usage > 0)    {  return $this->kFailed('Designation Assigned to User'); }

        
        // *********** Delete data from user_types table ****************

        $designation->where('id',$id)->delete();

        return $this->kSuccess('Designation Deleted Successfully');
    }


    //*************************** Route No. 1.5  Update Designation Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $designation = \App\Designation::Find($id);

        if(!$designation) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $designation_status = $designation->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\Designation::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
