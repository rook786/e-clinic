<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class WardAssignmentController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 29.1  Staff Ward Assignment  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'staff_id'              => 'required|array',
            'ward'                  => 'required|string|exists:ward_details,id',
            'from_date'             => 'required|date_format:Y-m-d',
            'to_date'               => 'required|date_format:Y-m-d',
        ],
        [
            'ward.exists'               => 'Ward Does not Exist',
            'from_date.date_format'     => 'Invalid From Date Format',
            'to_date.date_format'       => 'Invalid To Date Format',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $input['staff_id'] = implode(',',$input['staff_id']);

        $ward_assignment = \App\StaffWardAssignment::create($input);

        if($ward_assignment == '')                                {   return $this->kFailed('Unable To Assign Ward');  }

        return $this->kSuccess('Ward Assigned to Staff Successfully'); 
    }


    
    //*************************** Route No. 29.2  List all Ward Assignments ********************************



    public function get_list()
    {

        $per_page       =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby        =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order          =   $this->validate_var(@$_GET['order'], 'DESC');
        $staff_id       =   $this->validate_var(@$_GET['staff_id'], '');
        $date           =   $this->validate_var(@$_GET['date'], '');
        
               
        $model      =   new App\StaffWardAssignment;
              
        if($staff_id != '' || $staff_id != null)
        {   
            $model = DB::table('ward_assignments')
            ->whereRaw('FIND_IN_SET(?,staff_id)', $staff_id);  
        }

        if($date != '' || $date != null)
        {   
            $model = $model->whereDate('from_date','<=',$date)->whereDate('to_date' ,'>=',$date);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        foreach($result as $new_result)
        {
            $new_result->ward_name = \App\WardDetail::where('id',$new_result->ward)->first()->ward_name;
            
            if($new_result->from_date != '')
            {
                $new_result->formatted_from_date = date('M d, Y',strtotime($new_result->from_date));
            }

            if($new_result->to_date != '')
            {
                $new_result->formatted_to_date = date('M d, Y',strtotime($new_result->to_date));
            }

        }

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Assignment Found');   }
                
        return $this->kSuccess('Ward Assignment List Fetched Successfully',$result);
    }


    //*************************** Route No. 29.3  Update Staff Ward Assignment  ********************************


    public function update(Request $request, $id)
    {
         
        $ward_assignment = \App\StaffWardAssignment::Find($id);

        if(!$ward_assignment) { return $this->kFailed('Invalid Data'); }

               
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'staff_id'              => 'required|array',
            'ward'                  => 'required|string|exists:ward_details,id',
            'from_date'             => 'required|date_format:Y-m-d',
            'to_date'               => 'required|date_format:Y-m-d',
        ],
        [
            'ward.exists'               => 'Ward Does not Exist',
            'from_date.date_format'     => 'Invalid From Date Format',
            'to_date.date_format'       => 'Invalid To Date Format',
        ]);


        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $input['staff_id'] = implode(',',$input['staff_id']);

        $ward_assignment = $ward_assignment->update($input);

        return $this->kSuccess('Staff Ward Assignment Updated Successfully');
    }


    //*************************** Route No. 29.4  Delete Staff Ward Assignment  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $ward_assignment = \App\StaffWardAssignment::Find($id);

        if(!$ward_assignment) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from staff_ward_assignments table ****************

        $ward_assignment->where('id',$id)->delete();

        return $this->kSuccess('Staff Ward Assignment Deleted Successfully');
    }

}
