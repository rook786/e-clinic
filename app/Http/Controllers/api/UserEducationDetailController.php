<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class UserEducationDetailController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 25.1  Create User Education Detail  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'   =>  'required|exists:users,id',
            'details'   =>  'required|array',
            'details.*.course'          => 'required|string',
            'details.*.university'      => 'required|string',
            'details.*.year_of_passing' => 'required|string',
            'details.*.marks_obtained'  => 'required|string',
            'details.*.total_marks'     => 'required|string',
        ],
        [
            'user_id.exists'   => 'User Does not Exist',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $records_array = array();

        $details_array = $input['details'];

        foreach($details_array as $detail_array) 
        {
            $record = ['user_id' => $input['user_id'], 'course' => $detail_array['course'], 'university' => $detail_array['university'], 'enrollment_no' => $detail_array['enrollment_no'], 'year_of_passing' => $detail_array['year_of_passing'], 'marks_obtained' => $detail_array['marks_obtained'], 'total_marks' => $detail_array['total_marks'], 'division' => $detail_array['division'], 'grade' => $detail_array['grade']];

            $records_array[]    =   $record;

        }

        $user_education_detail = \App\UserEducationDetail::insert($records_array);

        if($user_education_detail == '')                                {   return $this->kFailed('Unable To Create User Education Detail');  }

        return $this->kSuccess('User Education Detail Created Successfully'); 
    }


    
    //*************************** Route No. 25.2   List User Education Detail  ********************************


    public function get_list()
    {

        $per_page           =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby            =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order              =   $this->validate_var(@$_GET['order'], 'DESC');
        $user_id               =   $this->validate_var(@$_GET['user_id'], '');
       
               
        $model      =   new App\UserEducationDetail;
              
        if($user_id != '' || $user_id != null)
        {   
            $model = $model->where('user_id' , $user_id);  
        }
    
        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page)->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No User Education Detail Found');   }
                
        return $this->kSuccess('User Education Detail Fetched Successfully',$result);
    }


    //*************************** Route No. 25.3  Update User Education Detail  ********************************


    public function update(Request $request, $id)
    {
         
        $user_education_detail = \App\UserEducationDetail::Find($id);

        if(!$user_education_detail) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'user_id'         =>  'required|exists:users,id',
            'course'          => 'required|string',
            'university'      => 'required|string',
            'year_of_passing' => 'required|string',
            'marks_obtained'  => 'required|string',
            'total_marks'     => 'required|string'
        ],
        [
            'user_id.exists'   => 'User Does not Exist',
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $user_education_detail = $user_education_detail->update($input);

        return $this->kSuccess('User Education Detail Updated Successfully');
    }


    //*************************** Route No. 25.4  Delete User Education Detail  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $user_education_detail = \App\UserEducationDetail::Find($id);

        if(!$user_education_detail) { return $this->kFailed('Invalid Data'); }
        
                       
        // *********** Delete data from user_educational_details table ****************

        $user_education_detail->where('id',$id)->delete();

        return $this->kSuccess('User Education Detail Deleted Successfully');
    }

}
