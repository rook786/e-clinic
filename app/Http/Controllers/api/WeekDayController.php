<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class WeekDayController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 6.1  Create Week Day  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:week_days,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Week Day Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $week_day = \App\WeekDay::create($input);

        if($week_day == '')                                {   return $this->kFailed('Unable To Create Week Day');  }

        return $this->kSuccess('Week Day Created Successfully'); 
    }


    
    //*************************** Route No. 6.2  Get Week Day List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\WeekDay;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Week Day Found');   }
                
        return $this->kSuccess('Week Day List Fetched Successfully',$result);
    }


    //*************************** Route No. 6.3  Update Week Day  ********************************


    public function update(Request $request, $id)
    {
         
        $week_day = \App\WeekDay::Find($id);

        if(!$week_day) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:week_days,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Week Day Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $week_day = $week_day->update($input);

        return $this->kSuccess('Week Day Updated Successfully');
    }


    //*************************** Route No. 6.4  Delete Week Day  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $week_day = \App\WeekDay::Find($id);

        if(!$week_day) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Week Day ****************

        // $week_day_usage = \App\OpdDetail::where('week_day',$id)->count();

        // if($week_day_usage > 0)    {  return $this->kFailed('Week Day Assigned to User'); }

        
        // *********** Delete data from week_days table ****************

        $week_day->where('id',$id)->delete();

        return $this->kSuccess('Week Day Deleted Successfully');
    }


    //*************************** Route No. 6.5  Update Week Day Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $week_day = \App\WeekDay::Find($id);

        if(!$week_day) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $week_day_status = $week_day->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\WeekDay::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
