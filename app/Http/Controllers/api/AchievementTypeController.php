<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Traits\trait_functions;
use Validator;
use App;


class AchievementTypeController extends Controller
{
    use response, trait_functions;

    
    //*************************** Route No. 11.1  Create Achievement Type  ********************************
    
    
    public function store(Request $request)
    {
        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:achievement_types,title|string|min:2'
            
        ],
        [
            'title.unique' => 'Achievement Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }

        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $achievement_type = \App\AchievementType::create($input);

        if($achievement_type == '')                                {   return $this->kFailed('Unable To Create Achievement Type');  }

        return $this->kSuccess('Achievement Type Created Successfully'); 
    }


    
    //*************************** Route No. 11.2  Get Achievement Type List  ********************************



    public function get_list()
    {

        $per_page   =   $this->validate_var(@$_GET['per_page'], 20); 
        $orderby    =   $this->validate_var(@$_GET['orderby'], 'created_at');
        $order      =   $this->validate_var(@$_GET['order'], 'DESC');
        $status     =   $this->validate_var(@$_GET['status'], '');
               
        $model      =   new App\AchievementType;
              
        if($status != '' || $status != null)
        {   
            $model = $model->where('status' , $status);  
        }

        $model      =   $model->orderBy($orderby,$order);

        $result     =   $model->paginate($per_page,['id','title','identifier','status'])->appends(request()->query());

        if(sizeof($result)== 0)                                 {   return $this->kFailed('No Achievement Type Found');   }
                
        return $this->kSuccess('Achievement Type List Fetched Successfully',$result);
    }


    //*************************** Route No. 11.3  Update Achievement Type  ********************************


    public function update(Request $request, $id)
    {
         
        $achievement_type = \App\AchievementType::Find($id);

        if(!$achievement_type) { return $this->kFailed('Invalid Data'); }

        
        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'title'  => 'required|string|unique:achievement_types,title,'.$id.'|min:2'
            
        ],
        [
            'title.unique' => 'Achievement Type Already Exist'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }


        $input = $request->all();

        $identifier = strtolower(str_replace(' ', '_', $input['title']));

        $input['identifier'] = $identifier;

        $achievement_type = $achievement_type->update($input);

        return $this->kSuccess('Achievement Type Updated Successfully');
    }


    //*************************** Route No. 11.4  Delete Achievement Type  ********************************
    
    
    public function destroy(Request $request, $id)
    {
        $achievement_type = \App\AchievementType::Find($id);

        if(!$achievement_type) { return $this->kFailed('Invalid Data'); }
        
        
        // // *********** Check for usage of Achievement Type ****************

        // $achievement_type_usage = \App\UserAchievement::where('achievement_type',$id)->count();

        // if($achievement_type_usage > 0)    {  return $this->kFailed('Achievement Type Assigned to User'); }

        
        // *********** Delete data from achievement_types table ****************

        $achievement_type->where('id',$id)->delete();

        return $this->kSuccess('Achievement Type Deleted Successfully');
    }


    //*************************** Route No. 11.5  Update Achievement Type Status ********************************


    public function update_status(Request $request,$id)
    {
        
        $achievement_type = \App\AchievementType::Find($id);

        if(!$achievement_type) { return $this->kFailed('Invalid Data'); }

        // *********** Check for required fields ****************

        $validator=Validator::make($request->all(), [

            'status'  => 'required|in:0,1'
            
        ],
        [
            'status.in' => 'Invalid Status'
        ]);

        if($validator->errors()->all())
        {
            return $this->kFailed($validator->errors()->first());   
        }
        
        $achievement_type_status = $achievement_type->where('id',$id)->update([ 'status' => $request->status ]);

        $result = \App\AchievementType::where('id',$id)->get(['id','title','identifier','status']);


        if(sizeof($result) == 0)     {   return $this->kFailed('Unable to Update Status');  }
                                          
        return $this->kSuccess('Status Updated Successfully', $result);
    }


}
