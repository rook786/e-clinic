<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffLeave extends Model
{
    protected $fillable = ['user_id','leave_type','from_date','to_date','from_time','to_time','no_of_days','reason','replacement'];
}
