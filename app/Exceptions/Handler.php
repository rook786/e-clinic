<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

use Illuminate\Support\Facades\Storage;
use File;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        $error = FlattenException::create($exception);
        $statusCode = $error->getStatusCode($error);

        if($exception instanceof AuthorizationException || $exception instanceof AuthenticationException)
        {
            //return response()->json(['error’ => 'Unauthenticated’], 401);
            return response()->json(['status_code' => 1001, 'status_text' => 'failed',
            'message' => 'Unauthenticated', 'error' => 'Unauthenticated'], 401);
        }


        if ($statusCode == 500) //SERVER CRASH
        {
            $url = $request->path();
            $url = explode("/",$url);
            $port_name = reset($url);
            $path = storage_path()."app/500/";
            \File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $time=date('Y-m-d-h-i-s',strtotime(now()));
            $full_path = '500/'.$time.'.txt';
            Storage::put($full_path, $exception);
            return response()->json(['status_code' => 0, 'status_text' => 'failed',
            'message' => 'SERVER ERROR', 'error' => 'SERVER ERROR'], 500);
        }


        if ($statusCode == 404) //NOT FOUND
        {
            return response()->json(['status_code' => 0, 'status_text' => 'failed',
            'message' => 'NOT FOUND', 'error' => 'NOT FOUND'], 404);
        }


        if ($statusCode == 405) //METHOD NOT ALLOWED
        {
            return response()->json(['status_code' => 0, 'status_text' => 'failed',
                                    'message' => 'METHOD NOT ALLOWED', 'error' => 'METHOD NOT ALLOWED'], 405);
        }


        if ($statusCode == 429) //TOO MANY REQUESTS
        {
            return response()->json(['status_code' => 0, 'status_text' => 'failed',
                                    'message' => 'TOO MANY REQUESTS', 'error' => 'TOO MANY REQUESTS'], 429);
        }

        return parent::render($request, $exception);
    }
}
