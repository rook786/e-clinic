<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementType extends Model
{
    protected $fillable = ['title','identifier','status'];
}
