<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = ['user_id','date_of_birth','date_of_joining','current_address','permanent_address','whatsapp_no','alternate_email','alternate_mobile','practice_licence_no'];
}
