<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmbulanceType extends Model
{
    protected $fillable = ['title','identifier'];
}
