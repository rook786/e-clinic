<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementCategory extends Model
{
    protected $fillable = ['title','identifier','status'];
}
