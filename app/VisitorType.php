<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorType extends Model
{
    protected $fillable = ['title','identifier','status'];
}
