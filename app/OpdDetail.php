<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpdDetail extends Model
{
    protected $fillable = ['opd_type','week_day','user_id','open_time','close_time','break_start_time','break_end_time','venue','status'];
}
