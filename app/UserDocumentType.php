<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocumentType extends Model
{
   protected $fillable = ['title','identifier','status'];
}
